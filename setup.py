import setuptools

setuptools.setup(
    name="TexLink",
    version="0.0.17",
    author="Antongiacomo Polimeno",
    author_email="antongiacomo.polimeno@studenti.unimi.t",
    description="A small package that check links status inside a Tex document",
    packages=setuptools.find_packages(),
    install_requires=[
        'aiohttp==3.7.3',
        'TexSoup==0.3.1',
        'bibtexparser==1.2.0',
        'Jinja2==2.11.2',
        'colorful==0.5.4'
    ],
    package_data={'TexLink': ['Templates/*.html']},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points='''
    [console_scripts]
    texlink=TexLink.main:main
    ''',

    python_requires='>=3.6',
)
