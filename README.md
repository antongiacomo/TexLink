# TexLink

## Check your Latex Links!

### Get Started

```
git clone https://gitlab.com/antongiacomo/TexLink.git
pip install .
texlink -d <your_latex_source>
```

### Somethnig More

```
usage: texlink [-h] [-d DIRECTORY] [-o] [-g] [-r] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -d DIRECTORY, --directory DIRECTORY
                        Directory from which read files
  -o, --open            Open Browser with the report at the end
  -g, --generate        Generate HTML report
  -r, --recursive       Recurse in folder
  -s, --strict          Return code based on stats
```
