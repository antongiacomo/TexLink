import bibtexparser
from bibtexparser.bparser import BibTexParser

from TexLink.Extractors.extractor import Extractor
from TexLink.url import Url


class BibExtractor(Extractor):
    file_extension = ['.bib']

    def extract(self):
        """ Extract Urls from Bib file

        Returns:
           List(Url): Extracted URLs
        """
        parser = BibTexParser(common_strings=True)
        parser.ignore_nonstandard_types = False
        parser.homogenise_fields = False

        bib_database = bibtexparser.loads(self.doc, parser)

        self.urls = [Url(v.get('url'), file=self.filename)
                     for v in bib_database.entries if 'url' in v.keys()]
