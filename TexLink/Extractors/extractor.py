from abc import abstractmethod
from typing import List

from TexLink.url import Url


class Extractor:
    doc: str
    urls: List[Url]
    filename: str
    file_extension: List[str] = []

    def __init__(self, filename: str):
        '''On init it opens the file, read it, and call the exctract method on it'''
        self.filename = filename

        with open(self.filename) as f:
            self.doc = f.read()

        self.extract()

    def count(self) -> int:
        '''It counts the extracted URLs
        Returns:
            int: The count
        '''

        return self.urls.count

    @ abstractmethod
    def extract(self) -> List[Url]:
        ''' Extract Urls, this method is implemented in the extender

       Returns:
          List(Url): Extracted URLs
       '''
