from pathlib import Path
from typing import List

from TexLink.Extractors.bib_extractor import BibExtractor
from TexLink.Extractors.url_extractor import UrlExtractor
from TexLink.url import Url


class ExtractorAggregator:
    ''' Utility class, given a filename finds the right extractor and init it giving back the URLs list'''

    def forFile(filename) -> List[Url]:
        ''' Call the right extractor
        Returns:
            List(Url): the list of extracted URL
        '''
        extension = Path(filename).suffix
        extractors = [BibExtractor, UrlExtractor]
        for extractor in extractors:
            if extension in extractor.file_extension:
                return extractor(filename).urls
        return []
