import colorful as cf
from TexSoup import TexSoup

from TexLink.Extractors.extractor import Extractor
from TexLink.url import Url


class UrlExtractor(Extractor):
    file_extension = ['.tex']

    def extract(self):
        """ Builds TexSoup on a LaTex source, Extract Urls from the soup

        Returns:
           List(Url): Extracted URLs
        """
        print("Building Soup on {}".format(
            self.filename), end='...', flush=True)
        try:
            soup = TexSoup(self.doc)
            urls = soup.find_all("url")

            self.urls = [
                Url(url.text[0].strip(), soup.char_pos_to_line(url.position), file=self.filename) for url in urls
            ]

            print("Built")
        except (EOFError, AssertionError):
            self.urls = []
            print("{}Cannot Soup {}{}".format(
                cf.red, self.filename, cf.reset))
