import argparse
import glob
import os
import webbrowser

import colorful as cf
from jinja2 import Template

from TexLink.Extractors.extractor_aggregator import ExtractorAggregator
from TexLink.downloader import Downloader

try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources

from TexLink import Templates


def run(args):
    REPORT_NAME = "report.html"
    downloader = Downloader()
    DIRECTORY = args["directory"]
    url_extracted = []
    if os.environ.get('NO_COLOR', None) or args.get("no_color", False):
        cf.disable()

    for filename in glob.glob(DIRECTORY + '/**', recursive=args["recursive"]):
        url_extracted += ExtractorAggregator.forFile(filename)

    downloader.run(url_extracted)

    print(downloader.stats)
    if args["generate"]:
        print("Generating Report")
        template_string = pkg_resources.read_text(Templates, 'template.html')
        template = Template(template_string)
        open(REPORT_NAME, "w").write(template.render(
            urls=url_extracted, stats=downloader.stats))
        if args["open"]:
            print("Opening Report")
            webbrowser.open('file://{}/{}'.format(os.getcwd(), REPORT_NAME))
            print("See You!")

    exit(downloader.stats.offline > 0)


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument(
        'directory', help="Directory from which read files", nargs='?', default='.')
    ap.add_argument(
        '-o', '--open', help="Open Browser with the report at the end", action='store_true')
    ap.add_argument('-g', '--generate',
                    help="Generate HTML report", action='store_true')
    ap.add_argument('-r', '--recursive',
                    help="Recurse in folder", action='store_true')
    ap.add_argument(
        '-s', '--strict', help="Return code based on stats", action='count', default=0)
    ap.add_argument(
        '--no_color', help="Disable Colored Output", action='store_true')
    args = vars(ap.parse_args())

    run(args)
