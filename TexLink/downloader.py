import asyncio
from typing import List

from TexLink.stats import Stats
from TexLink.url import Url


class Downloader:
    """Wrapper around asyncio, get an Url list"""
    stats: Stats = None
    urls: List[Url] = []

    async def __main(self) -> None:
        """Private method, iterate over urls and calls get each one of them"""
        ret = await asyncio.gather(*[url.get() for url in self.urls])

        print("\n{} links checked.".format(len(ret)))
        self.stats = Stats(ret)
        # ret = [x for x in ret if x is not None]

    def run(self, urls: List[Url]) -> None:
        """Runs urls get on each url in an asynchronous way"""
        self.urls = urls
        asyncio.run(self.__main())
