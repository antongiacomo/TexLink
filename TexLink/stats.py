from typing import List

import colorful as cf

from TexLink.url import Url


class Stats:
    offline: int = 0
    online: int = 0
    redirect: int = 0
    invalid: int = 0

    def __init__(self, ret: List[Url]) -> None:
        """Calculate stats of a returned sets of URL
        Args:
            ret List(URL):     list of URLs
        Returns:
            None
        """
        online = list(filter(lambda url: url.is_online, ret))
        offline = list(filter(lambda url: not url.is_online, ret))
        redirected = list(filter(lambda url: url.is_redirected, ret))
        invalid = list(filter(lambda url: not url.is_valid, ret))

        self.online = len(online)
        self.redirect = len(redirected)
        self.offline = len(offline)
        self.invalid = len(invalid)

    def __str__(self) -> str:
        """ String representation of stats """
        return "{green}{online} online{endc} - {yellow}{redirect} redirected{endc} - "\
            "{red}{offline} offline{endc} - {purple}{invalid} invalid{endc}".format(
                green=cf.green,
                online=self.online,
                red=cf.red,
                offline=self.offline,
                yellow=cf.yellow,
                redirect=self.redirect,
                purple=cf.cyan,
                invalid=self.invalid,
                endc=cf.reset
            )
