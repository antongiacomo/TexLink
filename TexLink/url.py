from typing import List, Tuple

import aiohttp
import colorful as cf
from aiohttp import InvalidURL
from yarl import URL

from TexLink.url_validator import UrlValidator


class Url:
    ''' Represent and URL with'''
    url: str
    ''' Path '''
    position: Tuple[int, int]
    ''' Position inside the original document, can be None '''
    file: str
    ''' File name from which the url has been extracted '''
    __status: int = 0
    ''' Status code '''
    __history: List = []
    """Redirection history"""

    def __init__(self, url, position=None, file=None):
        self.url = url
        self.position = position
        self.file = file

    @property
    def is_valid(self):
        """ Returns: True if the URL is valid False otherwise """
        return UrlValidator(self.url).validate()

    @property
    def is_online(self) -> bool:
        """ Returns: True if the URL is online False otherwise"""
        return self.__status == 200

    @property
    def is_redirected(self) -> bool:
        """ Returns: True if the original URL is redirected False otherwise"""
        return not self.__history == URL(self.url)

    def is_redirected_to(self) -> str:
        """ Returns: the last redirect path """
        if self.is_redirected:
            return str(self.__history.with_fragment(URL(self.url).fragment))
        return ""

    async def get(self):
        """ Check the URL status """
        try:
            async with aiohttp.ClientSession() as session:
                headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4)\
                     AppleWebKit/537.36 (KHTML, like Gecko)\
                     Chrome/75.0.3770.100 Safari/537.36'
                }
                async with session.get(url=self.url, headers=headers) as response:
                    await response.read()
                    self.__status = response.status
                    self.__history = response.url
                    print("{} {}(Position: {})".format(
                        cf.green("✔") if self.__status == 200
                        else cf.red("✘ (" + str(self.__status) + ")"),
                        self.url,
                        self.position)
                    )

        except InvalidURL:
            self.__status = 404
            print("{} {} URL in {} is invalid.".format(
                cf.red("✘"), self.url, self.file))
        except aiohttp.client_exceptions.ClientConnectorError as e:
            print(e)
            self.__status = 404
            print("{} URL {} in {} cannot connect.".format(
                cf.red("✘"), self.url, self.file))

        except Exception as exception:
            print("{} Something wrong during get url {} in {}  due to {}.".format(
                cf.red("✘"), self.url, self.file, exception.__class__))
        finally:
            return self
