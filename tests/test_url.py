import asyncio
import unittest

from TexLink.url import Url


class TestDownloader(unittest.TestCase):
    def setUp(self) -> None:
        self.url = Url("http://google.it")
        self.fullUrl = Url("https://www.google.it/?gws_rd=ssl")

    def test_url(self):
        self.assertFalse(self.url.is_online)
        self.assertEqual(self.url.url, "http://google.it")

    def test_url_get(self):
        self.assertFalse(self.url.is_online)
        asyncio.run(self.url.get())
        self.assertTrue(self.url.is_online)

    def test_url_get_redirected(self):
        self.assertFalse(self.url.is_online)
        asyncio.run(self.url.get())
        self.assertTrue(self.url.is_online)
        self.assertTrue(self.url.is_redirected)
        self.assertEqual(self.url.is_redirected_to(),
                         "https://www.google.it/?gws_rd=ssl")

    def test_url_get_not_redirected(self):
        asyncio.run(self.fullUrl.get())
        # self.assertFalse(self.fullUrl.is_redirected)
        self.assertEqual("",self.fullUrl.is_redirected_to())

    def test_url_get_not_position(self):
        asyncio.run(self.fullUrl.get())

        self.assertIsNone(self.fullUrl.position)

    def test_url_get_position(self):
        self.fullUrl = Url(self.fullUrl.url, 55)
        asyncio.run(self.fullUrl.get())

        self.assertEqual(self.fullUrl.position, 55)

    def test_deep_redirect(self):
        self.fullUrl = Url(
            "https://help.github.com/en/actions/reference/workflow-syntax-for-github-actions#jobsjob_idstepsuses")
        asyncio.run(self.fullUrl.get())
        self.assertEqual(self.fullUrl.is_redirected_to(),
                         "https://docs.github.com/en/free-pro-team@latest/actions/reference/workflow-syntax-for-github-actions#jobsjob_idstepsuses")

    # def test_timeout(self):
    #    self.fullUrl = Url("http://www.google.com:81/")
    #    asyncio.run(self.fullUrl.get())
    #    self.assertFalse(self.fullUrl.is_online)


if __name__ == '__main__':
    unittest.main()
