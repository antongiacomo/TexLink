from TexLink.url_validator import UrlValidator


def test_validator():
    bad_list = ["http://example/", "/foo", "/", "", "google.it", "www.google.it"]
    for bad in bad_list:
        assert not UrlValidator(bad).validate()
    assert UrlValidator("http://google.it").validate()
