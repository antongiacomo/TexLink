import unittest

from TexLink.downloader import Downloader
from TexLink.url import Url


class TestDownloader(unittest.TestCase):

    def setUp(self):
        self.downloader = Downloader()

    def test_downloader(self):
        urls = [Url("google.it")]
        self.downloader.run(urls)
        self.assertFalse(urls[0].is_online)

    def test_downloader2(self):
        urls = [Url("http://google.it")]
        self.downloader.run(urls)
        self.assertTrue(urls[0].is_online)

    def test_downloader3(self):
        urls = [Url("http://google.it"), Url("google.it"),
                Url("https://www.geeksforgeeks.org/constructors-in-python/")]
        self.downloader.run(urls)
        self.assertTrue(urls[0].is_online)
        self.assertFalse(urls[1].is_online)
        self.assertTrue(urls[2].is_online)


if __name__ == '__main__':
    unittest.main()
