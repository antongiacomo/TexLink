import os
import sys
import unittest
from io import StringIO
from unittest.mock import patch

from TexLink.main import run


class TestDownloader(unittest.TestCase):
    @patch.dict(os.environ, {"NO_COLOR": "."})
    def test_downloader(self):
        saved_stdout = sys.stdout

        try:
            out = StringIO()
            sys.stdout = out
            args = {'directory': 'tests/Integration', 'open': False,
                    'generate': False, 'recursive': False, 'verbose': 0}
            with self.assertRaises(SystemExit) as cm:
                run(args)
                self.assertEqual(cm.exception.code, 1)

            output = out.getvalue().strip()

            self.assertIn('2 online', output)
            self.assertIn('4 redirected', output)
            self.assertIn('2 offline', output)
            self.assertIn('2 online', output)
            # self.assertIn('\033[92m',output)
            self.assertIn(
                '2 online - 4 redirected - 2 offline - 1 invalid', output)
        finally:
            sys.stdout = saved_stdout

    def test_colors_downloader(self):
        saved_stdout = sys.stdout
        import colorful as cf
        try:
            out = StringIO()
            sys.stdout = out
            args = {'directory': 'tests/Integration', 'open': False,
                    'generate': False, 'recursive': False, 'verbose': 0}
            with self.assertRaises(SystemExit) as cm:
                run(args)
                self.assertEqual(cm.exception.code, 1)

            output = out.getvalue().strip()

            self.assertIn(str(cf.green), output)
        finally:
            sys.stdout = saved_stdout
