\section{Ciclo di vita}\label{sec:methodology-life-cycle}
Rispetto alla certificazione della cloud, i test effettuati
nella certificazione di un processo si sviluppo cambiano nell'\emph{ordine} in cui i test possono essere
fatti. Infatti, nella cloud i test possono essere \emph{one-shot}, o \emph{stateless}, ma non è
necessariamente così in questo scenario. È possibile che proprietà rilevate in una certa fase del processo
influiscano su proprietà rilevate nelle fasi successive. Quindi, il fatto che il processo di sviluppo
sia effettivamente un processo costituito da più fasi influisce sul processo di certificazione.
Questa influenza si può esprimere in vari modi, ad esempio evitando di dover rilevare proprietà
nelle fasi successive, poiché già garantite, oppure no, da proprietà rilevate in fasi precedenti.
Oppure, vi potrebbero essere influenze anche tra proprietà appartenenti ad una stessa fase.

Si pensi all'esempio dei team C++ e Rust. Una modellazione alternativa di evidenze e proprietà
potrebbe considerare la proprietà \emph{linguaggio di programmazione}.
Questa proprietà si riflette anche su proprietà ulteriori, in particolare si può riflettere
nelle proprietà \emph{memory safety} e \emph{assenza di data race}. L'avere la proprietà \emph{linguaggio di programmazione}
con valore \emph{Rust}, infatti, porterebbe automaticamente al rispetto delle proprietà \emph{memory safety} e \emph{assenza di data race}.

Un altro esempio di influenza è il seguente. Supponendo che la fase di testing di uno dei due team
fallisca, la pipeline CI/CD viene interrotta, e non si procede al \emph{deployment} del codice
in produzione. Ciò automaticamente influisce sulle proprietà eventualmente rilevate nelle fasi successive
alla fase di deployment.

Ciò che cambia è il \emph{ciclo di vita del certificato}, poiché il rilascio, o rinnovo, di un certificato
può portare all'automatico rilascio, o rinnovo, di un altro certificato, ed il mancato rilascio (o rinnovo)
di un certificato può portare al mancato rilascio (o rinnovo) di un altro. Per modellare ciò,
si definiscono le seguenti.

\begin{definition}[Influenza]
    $\mathcal{I}: pe \in (p \times e \in E_p) \rightarrow pi \subseteq (p \times e \in E_p)$
\end{definition}

La funzione $\mathcal{I}$, data una coppia di proprietà ed evidenza a supporto di essa, ritorna
un insieme di coppie $($proprietà, evidenza a supporto$)$. In altre parole, data la coppia in input,
ritorna l'insieme di coppie su cui la coppia in input \emph{influisce}.

\begin{definition}[Invalidità]
    Sia $c_i$ un certificato il cui rinnovo fallisca,  l'insieme di certificati da revocare è definito
    come $\mathcal{R} = \{c_k\ \forall c_k\ (c_k.\overline{p}, c_k.\overline{ev}) \in \mathcal{I}(c_i.\overline{p}, c_i.\overline{ev}) \}$ 
\end{definition}

In altre parole, dato un certificato il cui rinnovo per qualche ragione fallisca, allora anche tutti
i certificati che hanno una proprietà, su cui la proprietà del primo influisce, non possono essere rinnovati,
o devono essere considerati come non più validi.

Quindi, la metodologia di certificazione richiede, una volta che un certificato fallisce il rinnovo,
di controllare tutti gli altri certificati, e di \emph{invalidare} tutti quelli su cui il primo
certificato influisce. In maniera simile, si può dare la definizione di insieme di certificati
da rinnovare automaticamente, dato un certificato il cui rinnovo va a buon fine.

Durante le fasi di rinnovo, il processo di certificazione può sfruttare il concetto di \emph{influenza}
per rinnovare, o non rinnovare, automaticamente più certificati. Invece, durante la fase di primo rilascio,
si preferisce adottare un approccio più conservativo, imponendo di rilasciare il certificato
mediante la verifica diretta di evidenze.

Ciò si presta ampiamente all'uso di una pipeline. Se si sceglie, ad esempio, di invalidare tutti
i certificati inerenti ad una certa fase se fallisce il rinnovo di un certificato in una fase precedente,
è possibile sfruttare la funzione di \emph{influenza}. Per farlo, si definiscono le proprietà
delle fasi $x+1$ come dipendenti dalle proprietà della fase $x$.
