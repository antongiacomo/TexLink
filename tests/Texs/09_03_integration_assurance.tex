\section{Integrazione di un \emph{job} di assurance}\label{sec:case-study-integration}
Affinché la probe \probeservicetime\ possa essere integrata in Moon Cloud, è necessario
prima svolgere una fase di assurance del processo di sviluppo seguito.
Tale fase viene effettuata integrando le attività di Moon Cloud
all'interno della pipeline CI/CD del controllo esterno. In particolare, si esegue
la \aer\ \aerrustcheck, per la scansione delle dipendenze Rust di \probeservicetime, con
l'obiettivo di certificare la proprietà \emph{assenza di problemi nelle dipendenze del progetto}.

\subsection{Preparazione}\label{subsec:case-study-use-integration}
Per poter usufruire dei servizi di Moon Cloud in una pipeline Gitlab è sufficiente
importare il file CI/CD per Gitlab messo a disposizione da Moon Cloud, e
configurare una serie di variabili d'ambiente (Capitolo~\ref{cicd-native}).
Le variabili d'ambiente necessarie all'uso di \aerrustcheck\ sono state configurate come segue.

\begin{itemize}
    \item \envvar{MOONCLOUD\_SDK\_TARGET\_NAME}: il nome da assegnare al target che viene creato durante il \gitlabjob.
    In questo caso, si assume sia \emph{service\_response\_time}.
    \item \envvar{MOONCLOUD\_SDK\_PROJECT\_ID}: l'ID del \mooncloudproject\ entro cui il target va inserito.
    Questo parametro dipende da ciascun utente. In questo caso, si assume sia $73$.
    \item \envvar{MOONCLOUD\_SDK\_ZONE\_ID}: l'ID della \mooncloudzone\ entro cui il target va inserito.
    Questo parametro dipende da ciascun utente. In questo caso, si assume sia $91$.
    \item \envvar{MOONCLOUD\_SDK\_TARGET\_TYPE}: specifica la tipologia di target che viene creato durante il
    \gitlabjob, in questo caso un \emph{artifact}, poiché l'obiettivo dell'analisi sono dei file, non un host.
    \item \envvar{MOONCLOUD\_SDK\_AER\_ID}: specifica l'ID della \aer\ da eseguire. In questo caso, \aerrustcheck\ 
    ha ID $15$.
    \item \envvar{MOONCLOUD\_SDK\_UER\_NAME}: il nome da assegnare alla \er\ che viene creata. In questo caso, si
    utilizza il nome \emph{service\_response\_time\_dependency\_check}.
    \item \envvar{MOONCLOUD\_SDK\_UER\_CONTROLS}: il \emph{payload} da passare alla \er\ che viene creata.
    L'\aer\ in questione riceve tre input, di cui due sono il contenuto di due file. Per gestire comodamente
    situazioni come queste, \mooncloudcontainer\ fornisce la sintassi tra doppie parentesi angolari (\codeline{52}),
    descritta in dettaglio in Sezione~\ref{subsec:cicd-native-deployment-use}. In sintesi, ciò che
    viene racchiuso tra doppie parentesi angolari viene considerato come il nome di un file, che
    \mooncloudcontainer\ deve sostituire con il contenuto del file stesso.
\end{itemize}

Infine, occorre anche definire le variabili \envvar{MOONCLOUD\_SDK\_USERNAME} e \envvar{MOONCLOUD\_SDK\_PASSWORD},
necessarie per autenticarsi con le API di Moon Cloud. Si assume che tali variabili non siano scritte
direttamente nel file YAML, ma salvate, ad esempio, come variabili segrete a livello di \emph{repository}.

Il nuovo file \gitlabci\ della probe \probeservicetime\ si presenta come segue.
\inputminted[highlightlines={33-52}]{yaml}{code/09_03_service_response_time_new_gitlabci.yml}

La definizione delle variabili d'ambiente è tutto ciò che è necessario per l'utilizzo di Moon Cloud
in una pipeline.

\subsection{Esecuzione}\label{subsec:case-study-integration-execution}
Una volta aggiornato il file \gitlabci\ come mostrato, il team DevOps che sviluppa la probe \probeservicetime\ 
può fare un \emph{commit} delle nuove modifiche, ed un \emph{push}  per caricare le modifiche al
codice nella propria \emph{repository} Gitlab. Il \emph{push} fa scattare l'esecuzione della pipeline CI/CD.
Il primo \gitlabjob, \gitlabjobname{build}, è identico al precedente, come anche il successivo,
\gitlabjobname{test}; ciò che cambia è l'esecuzione del \gitlabjob\ di Moon Cloud, ovvero \gitlabjobmooncloud.

Il risultato dell'evaluation \aerrustcheck\ dà esito negativo. Tale risultato è dovuto al fatto
che vi è uno \emph{warning} nelle dipendenze usate dal progetto \probeservicetime. In particolare,
esso è legato al fatto che una delle librerie utilizzate non è più mantenuta. Una versione semplificata
del risultato dell'esecuzione dell'\aer\ sul progetto \probeservicetime\ viene mostrato di seguito.

Poiché l'\aer\ è fallita, la proprietà di \emph{assenza di problemi nelle dipendenze del progetto} non
può essere certificata. Per tale ragione, la probe \probeservicetime, così come è, non può essere
integrata nei controlli forniti da Moon Cloud. Inoltre, poiché il \gitlabjob\ che esegue tale verifica
è fallito, i successivi \gitlabjob\ non vengono eseguiti. Nel dettaglio, non viene eseguito il
\gitlabjob\ di \gitlabjobname{deploy}, e la versione attuale della probe \emph{non} viene rilasciata.

L'output dell'esecuzione della probe dell'\aer \aerrustcheck, in versione semplificata, è il seguente.
\inputminted[highlightlines={9, 17, 20}]{json}{code/09_03_service_response_time_assurance.json}

\subsubsection{Discussione}\label{subsubsec:case-study-integration-discussion}
È importante notare le differenze rispetto alla pipeline precedente.
Nella versione senza l'uso di Moon Cloud, era sufficiente passare la fase di unit testing per
arrivare in produzione. Con l'introduzione di attività di assurance, in questo caso relative
all'assenza di problemi nelle dipendenze, diventa necessario passare anche questa ulteriore fase
per poter arrivare a rilasciare una nuova versione. Infatti, poiché non è stato possibile certificare
l'assenza di problemi nelle dipendenze, non è stato possibile procedere alla fase di rilascio.

Si noti che l'attività di assurance effettuata è una delle tante possibili. Essendo
agnostico sulla specifica attività da eseguire, è possibile sfruttare Moon Cloud in diverse fasi
della pipeline, per certificare più proprietà. Ad esempio, per certificare che la fase di testing
è stata fatta, oppure per certificare il raggiungimento di un certo \emph{coverage}, è possibile
configurare ulteriori \aer\ che lavorino su altri artefatti, come l'output della fase di unit testing.
