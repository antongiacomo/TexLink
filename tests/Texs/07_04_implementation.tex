\section{Implementazione}\label{sec:trustworthy-implementation}

\subsection{\testcase\ e \testenv}\label{subsec:trustworthy-implementation-testcase-testenv}
Un singolo \testcase\ per un controllo corrisponde in concreto ad una directory, contenente tutti
i file necessari, ovvero \testinputjson, \testoutputjson\ (eventualmente), \testjson\ (eventualmente),
\dockercomposefile\ (eventualmente). Grazie alla presenza di valori di default per \testconf,
l'unico file la cui presenza è necessaria è \testinputjson.
Un \testenv\ è un insieme di \testcase, e corrisponde in concreto ad una directory contenente le
directory dei \testcase. Ogni directory di \testcase, all'interno della directory
del \testenv, deve avere un nome che inizi con il prefisso \testcasedirectory.
Un \testenv\ costituisce, infine, un progetto a sé nel \emph{repository} Gitlab di Moon Cloud.
Un esempio è mostrato in Figura~\ref{fig:testenv-example-dir-tree}, in cui il \testenv\ è composto
da due directory, ciascuna delle quali modella un \testcase.

\begin{figure}[t]
    %\centering
    \dirtree{%
.1 test\_1.
.2 input.json.
.2 docker-compose.yml.
.2 output.json.
.2 test.json.
.1 test\_2.
.2 input.json.
.2 output.json. 
}
    \caption[Esempio di struttura di directory per un \testenv]{Esempio di struttura di directory per un \testenv.}
    \label{fig:testenv-example-dir-tree}
\end{figure}


Il primo \testcase, \filename{test\_1}, usa un target basato su \dockercompose, e sovrascrive
alcune delle configurazioni, ecco perché si utilizza anche il file \filename{config.json}.
Il secondo \testcase, invece, non usa un target basato su \dockercompose, ed utilizza
solo configurazioni di default. Queste sono le ragioni per cui non vi sono i file
\filename{config.json}\ e \filename{docker-compose.yml}.

\subsection{Target}\label{subsec:trustworthy-implementation-target}
Per realizzare il target di ciascun \testcase\ si è seguito un approccio basato su
Docker e \dockercompose~\ref{subsec:trustworthy-pre-req-docker-compose}. Questa scelta è stata fatta poiché in questo modo
si possono rispettare vari requisiti, in particolare i seguenti. 
\begin{description}
    \item[\reqtestmreproducibility] Tale requisito viene rispettato grazie all'utilizzo di Docker. Ciascun container, o insieme
    di container, garantisce la riproducibilità poiché viene eseguito ogni volta a partire
    da una immagine pulita e funzionante, e distrutto al termine del test, annullando ogni eventuale modifica
    frutto dell'esecuzione del test verso di essi.
    \item[\reqtestmeasyusage] Tale requisito viene rispettato, per ciò che concerne i target, grazie al basso
    numero di operazioni necessarie alla creazione di un nuovo target. 
    In particolare è sufficiente scrivere un nuovo \dockercomposefile\ (ed eventuali definizioni di ulteriori \filename{Dockerfile}).  
    \item[\reqtestmlightness] Tale requisito viene rispettato grazie alle caratteristiche intrinseche di Docker. Nel momento
    in cui occorre testare un controllo, i target di test vengono istanziati. Quando il testing è finito, è sufficiente
    fermare i container che sono stati eseguiti. \dockercompose\ rende le cose persino più facili, perché
    è possibile far partire e fermare \emph{tutti} i container con un unico comando.
\end{description}

Per ciascun \testcase\ va quindi definito un file \dockercomposefile, contenente la descrizione del target. Nel caso
in cui i servizi specificati in tale file richiedano del tempo per partire, si può configurare il \testexec\
con un periodo di attesa, per dar tempo al target di partire.
 
\subsection{\testexec}\label{subsec:trustworthy-implementation-testexec}
Il \testexec\ è la parte dell'infrastruttura di test dei controlli effettivamente esegue della computazione.
Essa, per ogni \testcase\ del \testenv, esegue i seguenti passi concreti.

\begin{description}
    \item[1. Creazione target di test] Per creare il target di test per il corrente \testcase\ l'esecutore
    esegue il comando \dockercomposeup\ specificando come file di orchestrazione quello specificato
    nel file \testconf. Nel caso in cui il file indicato non esista, questa fase non viene effettuata. 
    \item[2. Esecuzione test] Per eseguire il test per il corrente \testcase\ l'esecutore
    utilizza direttamente il comando \cmd{docker run}, specificando quale immagine occorre eseguire, ovvero
    quella del controllo oggetto di test. Successivamente, l'esecutore passa al container in esecuzione il contenuto
    del file \testinputjson\ via standard input. Poi, l'esecutore aspetta che il controllo termini l'esecuzione e
    ne recupera l'output da standard output, contenente sia il risultato Booleano sia gli \extradata.
    Infine, l'esecutore effettua una pulitura dell'output catturato, poiché contiene alcune informazioni non
    necessarie per il confronto successivo. 
    \item[3. Distruzione target di test] Se la fase \num{1} non è stata saltata, l'esecutore esegue il comando
    \dockercomposedown\ per fermare tutti i container che sono stati lanciati. 
    \item[4. Confronto] L'ultima fase è il confronto tra il contenuto del file \testoutputjson\ e l'output ritornato
    dall'esecuzione del controllo. Se il file \testoutputjson\ non esiste, la fase di confronto viene saltata, ed
    il test ha successo. Altrimenti, il \testcase\ in questione ha esito positivo solo se il confronto tra i due
    JSON conferma che siano uguali. 
\end{description}

La Figura~\ref{fig:trustworthy-control-sequence-diagram} mostra quanto descritto sotto forma di diagramma di
sequenza UML.
L'output finale dell'esecuzione del \testenv\ riporta tutti i \testcase\ che hanno avuto successo e quelli che
non l'hanno avuto. Esso ha successo solo se tutti i \testcase\ hanno avuto successo.
In concreto, il \testexec\ ritorna come \emph{exit code} $0$ se l'esecuzione ha avuto successo, $-1$ altrimenti.
Un \emph{exit code} negativo infatti indica una terminazione con errore, bloccando la pipeline.

\begin{figure}[t]
    \centering
    \includegraphics[scale=0.4]{img/05_04_ci_executor_uml.png}
    \caption[Diagramma di sequenza dell'esecuzione di un \testenv]{Diagramma di sequenza dell'esecuzione di un \testenv.}
    \label{fig:trustworthy-control-sequence-diagram}
\end{figure}

%\subsection{Implementazione del \testexec}\label{subsec:trustworthy-impl-real-testexec}
Le attività svolte dal \testexec\ sono abbastanza semplici. Si tratta di un \emph{for} sui \testcase, eseguendo
per ciascuno i comandi \dockercomposeup, esecuzione della probe, \dockercomposedown, e confronto dell'output.
Tuttavia, fin dall'inizio, si è deciso di non scrivere queste istruzioni direttamente nella shell di Linux, ma
di usare un linguaggio di programmazione \emph{vero e proprio}, in particolare si è scelto il linguaggio
Rust (Sezione~\ref{subsec:trustworthy-pre-req-rust}). Il linguaggio Rust consente di rispettare
i requisiti di \reqtesteasyimplemenentation\ e \reqtestextensibility.

%\textbf{TDO VEDERE DOVE METTERLO, MAGARI DOPO}
\noindent \textbf{Configurazione} Fin da subito si è progettato e realizzato il tool di \testexec\ pensando
al fatto che sarà eseguito all'interno di una pipeline, in particolare all'interno di un container Docker
appositamente definito. Una prassi tipicamente adottata è quella di passare opzioni di configurazioni ai vari \gitlabjob\
della pipeline mediante variabili d'ambiente, e così è stato fatto anche in questo caso. Nello specifico, si forniscono tre
variabili d'ambiente come segue.

\begin{description}
    \item[\envcitestimage] Contiene il nome dell'immagine Docker oggetto di test, ovvero il nome
    dell'immagine del controllo da testare. 
    \item[\envcitestcasedir] Contiene il percorso a cui si trova la directory che definisce il \testenv,
    ovvero la directory che contiene l'insieme di directory ciascuna delle quali modella un \testcase.
    \item[\envciexecutor] Contiene l'indirizzo IP dell'host verso cui i comandi \cmd{docker} e
    \cmd{docker-compose} saranno lanciati.  
\end{description}

La configurazione con variabili d'ambiente consente di rispettare il requisito di
\reqtestcommonint, poiché l'interfaccia d'uso è la stessa, cambia solo la definizione
di tali variabili.

Il software è composto da 6 file per una lunghezza complessiva, esclusa dai commenti e dallo unit testing,
di circa 507 righe di codice. Vi sono inoltre, approssimativamente, 350 righe di codice dedicate allo unit testing. 
In Rust ciascun file costituisce un modulo; il progetto è quindi organizzato come segue.

\begin{description}
    \item[\filename{main.rs}] Il modulo contenente il punto d'entrata del programma, ovvero la funzione \emph{main}.
    \item[\filename{lib.rs}] Il modulo contenente le definizioni dei tipi usati nel resto del software, unitamente
    alla  \emph{struct} che modella un \testenv\ ed alle funzioni che operano su di esso, ovvero le funzioni di
    più alto livello richiamate nel \emph{main}.
    \item[\filename{command.rs}] Il modulo contenente funzioni di utilità per l'esecuzione di comandi.
    \item[\filename{parse\_output.rs}] Il modulo contenente una semplice funzione responsabile della fase di pulizia
    dell'output di una probe.
    \item[\filename{test\_case\_exec.rs}] Il modulo contenente la \emph{struct} che modella un singolo \testcase,
    unitamente a vari metodi definiti su essa, i quali compiono le fasi necessarie all'esecuzione del singolo \testcase.  
    \item[\filename{utils.rs}] Il modulo contenente una serie di funzioni di utilità per lavorare con
    le variabili d'ambiente, tramite le quali si configura il software.
\end{description}

\subsubsection{Parti salienti}\label{subsec:trustworthy-impl-real-testexec-parts}
Di seguito si mostrano alcune parti salienti del software, con estratti di codice Rust.

\noindent \textbf{Modellazione del \testenv} Il \testenv\ è definito da una semplice \emph{struct} chiamata \proglangkey{Executor},
costituita dal nome dell'immagine del controllo da testare, e da un vettore contenente i \testcase, come segue.
\inputminted[firstline=1, lastline=7]{rust}{code/05_04_testenv_all.rs}
La funzione \proglangkey{new} crea un nuovo \proglangkey{Executor}, costruendo la lista di \testcase\ a
partire dalla directory indicata, come segue.
\inputminted[firstline=26, lastline=72]{rust}{code/05_04_testenv_all.rs}
Infine, la funzione \proglangkey{exec} esegue tutti i \testcase\ del \testenv in questione. Si tratta di un
semplice ciclo sul vettore di \testcase\ computato precedentemente. Tale ciclo non viene eseguito in
maniera concorrente, sebbene sia possibile. Questa scelta è stata dettata dalla relativa immaturità
del paradigma \emph{async/await} di Rust; una volta che tale stack sarà maturo questo ciclo sarà
eseguito concorrentemente.
\inputminted[firstline=78, lastline=107]{rust}{code/05_04_testenv_all.rs}
\noindent \textbf{Modellazione del \testcase} Il \testcase\ è definito da una \emph{struct} chiamata \proglangkey{TestCase}.
Essa contiene i parametri per l'esecuzione di \dockercompose, l'input da passare al controllo e l'eventuale
output per il confronto, come segue.
\inputminted[firstline=1, lastline=21]{rust}{code/05_04_testcase_all.rs}
La funzione \proglangkey{execute} esegue il singolo \testcase, prendendo come parametro in input
il nome dell'immagine del controllo, come segue.
\inputminted[firstline=25, lastline=69]{rust}{code/05_04_testcase_all.rs}
