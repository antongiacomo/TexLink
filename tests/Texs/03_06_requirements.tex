\section{Requisiti}\label{sec:methodology-requirements}
La metodologia formalizzata in Sezione~\ref{sec:methodology-methodology} viene implementata da un \emph{processo
di assurance} e da un \emph{framework di assurance}, il quale effettivamente esegue
le attività di certificazione.
Il processo ed il framework di partenza sono le soluzioni consolidate descritte in \cite{anisetti2016certification, AADG.TSC2017},
che devono essere adattate per la nuova metodologia.
A tale scopo, si definiscono quali sono i requisiti del processo di
assurance (Sezione~\ref{subsec:methodology-process-requirements}), per poi definire i requisiti del framework
che implementa tale processo (Sezione~\ref{subsec:methodology-framework-requirements}).
Essi hanno un duplice scopo: da un lato allineano l'implementazione con la metodologia teorica, dall'altro
fan sì che le attività di assurance siano inseribili in una pipeline CI/CD rispettandone
i due pilastri principali, ovvero $i)$ descrizione dei passi da eseguire in file di testo e $ii)$ esecuzione
in container. Sono riassunti in Tabella \ref{tbl:req-summary-process} e \ref{tbl:req-summary-framework}.

\subsection{Requisiti del processo}\label{subsec:methodology-process-requirements}
I requisiti che il processo di assurance deve avere sono i seguenti~\cite{10.1145/3297662.3365827}.

\begin{description}
    \item[Modellazione del processo di sviluppo] Il processo di sviluppo, in particolare DevOps, deve essere
    preso in considerazione dal processo di certificazione, e deve essere parte di ciò che viene certificato.
    Infatti, il processo di sviluppo è una delle due dimensioni di cui si compone la metodologia di certificazione.
    Per abilitare ciò, il processo di certificazione deve essere adattato,
    non è sufficiente supportare nuovi tipi di target, cioè gli artefatti del processo di sviluppo
    (Sezione~\ref{sec:methodology-why-assurance-cannot-be-used-as-is}).
    \item[Valutazione passo per passo] Oltre ad includere il processo di sviluppo come parte di ciò che
    si certifica, il processo di certificazione deve poter certificare i diversi passi del processo di sviluppo,
    idealmente dovrebbe poter agire su ciascuno di essi.
    \item[Certificazione continua e trigger esterni] Per supportare la certificazione dell'intero processo di sviluppo,
    occorre che il processo di sviluppo stesso fornisca degli \emph{hook} per la verifica
    continua. Occorre anche che il processo di assurance supporti una verifica che non venga
    innescata solo dal caricamento del codice nella \emph{repository}, ma anche da eventi esterni.
    In particolare, occorre che cambiamenti nel contesto di sicurezza, ovvero cambiamenti
    nelle verifiche di sicurezza effettuate, possano innescare una nuova esecuzione del processo
    di assurance, usando le verifiche aggiornate.
    Si pensi ad esempio ad una verifica che le dipendenze di un progetto, in un certo linguaggio di programmazione,
    non contengano vulnerabilità note. Nel momento in cui vengono trovate nuove vulnerabilità in
    librerie usate da tale linguaggio, occorre ri-verificare il progetto, per assicurarsi che
    le nuove vulnerabilità scoperte non lo affliggano.
    Nel fare ciò, è possibile applicare il concetto di \emph{influenza}, per gestire il ciclo
    di vita dei vari certificati (Sezione~\ref{sec:methodology-life-cycle}).
    \item[Strumenti di verifica \emph{trustworthy}] La seconda dimensione di cui si
    compone la metodologia di certificazione è il processo di verifica, che modella
    \emph{come} la verifica viene fatta, cioè le garanzie che il framework di certificazione fornisce.
    La metodologia presentata offre una modellazione generica di questo concetto, tuttavia sarebbe
    preferibile che il framework fornisca le garanzie migliori possibile. In particolare,
    si richiede che gli strumenti di verifica siano \emph{trustworthy}, così che il team DevOps possa
    avere fiducia nei certificati rilasciati dal framework.
    \item[Malleabile] Il processo di assurance deve adattarsi a pipeline CI/CD diverse, e deve
    consentire al team DevOps la massima flessibilità. Il team deve poter aggiungere gli step
    concernenti la certificazione nel modo che ritengono migliore per la propria pipeline. Il processo
    di assurance deve quindi essere \emph{malleabile}, prestandosi a diversi casi d'uso con diversi requisiti
    e diversi strumenti DevOps.
\end{description}

\begin{table}[t]
    \centering
    \caption[Riassunto dei requisiti del \emph{processo di assurance}]{Riassunto dei requisiti del \emph{processo di assurance}.}
    \label{tbl:req-summary-process}
    %\resizebox{1.\textwidth}{!}{
    \begin{tabular}{|p{.4\textwidth}|p{.23\textwidth}|p{.24\textwidth}|}
        \hline
        \multirow{2}{*}{Nome requisito} &
        \multicolumn{2}{c|}{Scopo} \\\cline{2-3}
        & Allineamento tra metodologia e implementazione & Inserimento certificazione in pipeline CI/CD\\
        \hline
        \hline
        Modellazione del processo di sviluppo & \yes & \no \\
        \hline
        Valutazione passo per passo & \yes & \no \\
        \hline
        Certificazione continua e trigger esterni & \almost & \almost \\
        \hline
        Strumenti di verifica \emph{trustworthy} & \almost & \almost \\
        \hline
        Malleabile & \no & \yes \\
        \hline
    \end{tabular}
    %}
\end{table}

\subsection{Requisiti del framework}\label{subsec:methodology-framework-requirements}
Il framework di assurance che implementa il processo prima citato è esterno allo strumento DevOps.
Esso offre i propri servizi di certificazione al framework DevOps che gestisce
lo sviluppo, come Gitlab.
L'integrazione dei servizi di certificazione deve avvenire in maniera semplice.
Se l'\emph{overhead} per aggiungere le attività di certificazione
alla pipeline sono troppo elevate, probabilmente tali attività semplicemente non saranno effettuate.
In Sezione~\ref{sec:devsecops-gitlab} si è mostrato come in Gitlab sia estremamente facile aggiungere
delle verifiche di sicurezza, è sufficiente importare un secondo file nel file principale che descrive
la pipeline. Nell'integrare un framework di assurance in uno di DevOps, si vuole seguire questo stesso approccio.
Si definiscono quindi due serie di requisiti come segue. Una prima serie (\emph{evidence-based collection},
\trustcontrols\ e \emph{scalabilità}) si riferisce a requisiti
validi per qualsiasi framework di assurance. Una seconda serie (\apidriven, \cicdnative\ e \certcontrolstrigg)
modella invece requisiti peculiari per adattarsi a DevOps. Infine, vi è anche
un requisito non obbligatorio, detto \emph{certificazione distribuita}.

\subsubsection{Evidence-based collection}\label{subsubsec:methodology-framework-requirements-evidence-based-collection}
La metodologia di certificazione certifica il rispetto di proprietà non funzionali mediante
la raccolta di evidenze nel processo il sistema target (Definizione~\ref{def:methodology-evidence}).
Per \emph{evidenze} si intendono sia evidenze \emph{classiche}, come evidenze raccolte presso
host e sistemi cloud, sia artefatti del processo di sviluppo software.
Il framework che implementa tale metodologia di certificazione deve, quindi, supportare la raccolta di evidenze.

\subsubsection{\emph{Trustworthy} controls}\label{subsubsec:methodology-framework-requirements-trustworthy-controls}
La necessità di avere dei controlli \emph{trustworthy} è stata espressa nei requisiti del processo
come \emph{strumenti di verifica \emph{trustworthy}}.
Esso si concretizza nel framework che implementa tale processo, in cui
i \emph{controlli} sono definiti come gli strumenti di verifica usati dal framework.
Tali controlli devono essere accuratamente testati, e rilasciati solo e
soltanto nel momento in cui si è realmente confidenti del loro corretto funzionamento. Idealmente,
i controlli possono essere sviluppati in un processo CI/CD, e vi dovrebbe essere uno step
nella pipeline in cui si effettua il testing. Solo nel momento in cui la fase di test viene
superata con successo è possibile rilasciare una nuova versione di tale controllo.
L'avere dei controlli \emph{trustworthy} è modellabile tramite la seconda delle due dimensioni della
metodologia di certificazione (Sezione~\ref{subsec:methodology-methodology-process-certification}).

\subsubsection{Scalabilità}\label{subsubsec:methodology-framework-requirements-scalability}
Il framework di assurance deve essere scalabile, deve essere in grado di gestire un numero crescente
di valutazioni, senza che l'aumentare di tale numero impatti troppo sulle performance.

\subsubsection{API-driven}\label{subsubsec:methodology-framework-requirements-api-driven}
Poiché il framework di assurance è esterno al framework DevOps, occorre un modo
per poterne integrare le funzionalità. È necessario che il framework esponga delle API
per poter usufruire dei servizi esposti, in maniera programmatica.
Queste API, indipendentemente dal tipo di interfaccia esposta, devono seguire
uno standard, sia esso REST, SOAP, ecc\ldots
Nella scelta del paradigma da utilizzare, si dovrebbe tenere conto dei seguenti due
parametri.
\begin{description}
    \item[Conoscenza] Il paradigma deve essere ampiamente utilizzato e noto alla comunità di
    sviluppatori. Anche nel caso in cui la scelta di orienti su un paradigma che richiede
    un framework per l'utilizzo (ad esempio RPC o SOAP), occorre privilegiare l'utilizzo di
    framework ben noti. In questo modo, il team DevOps che deve integrare il framework di
    assurance si troverà a lavorare con del software che conosce bene. Ciò diminuisce
    lo sforzo di integrazione, il team DevOps non deve imparare una tecnologia che non conosce. 
    \item[Facilità d'uso] Il paradigma scelto deve essere facile da usare, indipendentemente
    dal fatto che esso sia già noto o meno. La facilità d'uso si riferisce anche alla quantità
    di software e librerie che è necessario coinvolgere per interfacciarsi con il framework di assurance.
    Ad esempio, l'uso di API HTTP (REST) è ben più semplice di RPC, poiché richiede solo l'uso
    di un client HTTP. Mentre per RPC, invece, tipicamente, occorre definire un \emph{IDL (Interface Description
    Language)} ed un compilatore che generi codice a partire da esso.
\end{description}

Il requisito prevede due ulteriori livelli, l'avere una documentazione standard,
ed una libreria di alto livello.

\noindent \textbf{Documentazione standard} Limitarsi ad esporre delle API di qualche tipo non è sufficiente.
Infatti, è ragionevole ritenere che, per completare una attività di certificazione, potrebbero essere necessarie
diverse interazioni con il framework di assurance, ed il framework stesso potrebbe risultare complesso da utilizzare.
Tenuto conto di ciò, per facilitare l'integrazione del framework in una pipeline, occorre che l'interfaccia
esposta dal framework di assurance fornisca una documentazione esaustiva.
Inoltre, non solo l'interfaccia esposta dal framework dovrebbe seguire uno standard, ma anche la documentazione
stessa. Ciò significa che la documentazione non deve essere del semplice testo, ma dovrebbe seguire
un certo formalismo, a seconda dell'interfaccia usata. Ad esempio, per un'interfaccia di tipo REST,
esiste lo standard di documentazione chiamato \emph{OpenAPI}~\cite{openapi2014openapi}.

\noindent \textbf{Libreria di alto livello} Per facilitare ulteriormente l'integrazione e meglio rispettare il requisito di
\emph{malleabilità} (Sezione~\ref{subsec:methodology-process-requirements})
è necessario un passo ulteriore. Proprio perché il numero di interazioni con il framework di certificazione è
potenzialmente alto, ed eventualmente complicato, occorre fornire una libreria di più alto livello, un
\emph{SDK (Software Development Kit)}. L'SDK non è altro che un \emph{layer} superiore alle chiamate all'interfaccia
definita dalle API. Per accomodare i peculiari requisiti di ogni pipeline CI/CD, questa
libreria dovrebbe essere fornita in più linguaggi di programmazione, in modo che ciascuno possa
scegliere la versione che preferisce. Idealmente, ogni team DevOps può utilizzare la versione della libreria
scritta con il linguaggio di programmazione con cui si trova meglio. Questa libreria deve essere sempre
allineata con l'interfaccia offerta dal servizio di certificazione.
Il team DevOps può utilizzare la libreria per inserirla nei propri workflow, componendo le funzionalità
offerte come si preferisce, grazie all'espressività data da un linguaggio di programmazione.

Avere i servizi del framework di certificazione richiamabili mediante API consente di colmare
una delle lacune indicate in Sezione~\ref{sec:methodology-why-assurance-cannot-be-used-as-is},
cioè la \emph{centralizzazione del framework di assurance}. Ora, infatti, le attività
di certificazione diventano esterne, e sono uno dei vari strumenti usati nel framework
che gestisce lo sviluppo, che mantiene la sua centralità.

\subsubsection{CI/CD-native}\label{subsubsec:methodology-framework-requirements-cicd-native}
I requisiti fin'ora presentati ancora non sono sufficienti per paragonare un framework che li rispetti
con quanto offerto dallo stato dell'arte odierno, come Gitlab.
L'esecuzione dei vari step della pipeline avviene, infatti, in container.
Il framework di certificazione è un servizio terzo rispetto al framework DevOps in cui viene inserito, e con il
requisito di \cicdnative\ si intende l'offrire le funzionalità del framework di certificazione
mediante container.

L'immagine del container può essere realizzata con l'SDK del framework di certificazione, in un linguaggio
di programmazione a scelta. Grazie a ciò, è possibile un definire un file di CI/CD che espone uno o più step
della pipeline, che possono essere richiamati dal file principale della pipeline.
La configurazione della fase di certificazione, com'è prassi in CI/CD, avviene mediante variabili d'ambiente.
Nel file principale della pipeline il team DevOps potrà configurare i vari aspetti degli step di
certificazione utilizzando delle variabili d'ambiente. Dietro le quinte il container utilizzerà l'SDK
per orchestrare la fase di certificazione. Per brevità, tale container contenente l'SDK viene chiamato
\clientcontainer.

%\subsubsection{Requisiti}
Il \clientcontainer\ deve soddisfare una serie di requisiti, nell'ottica di fornire un'integrazione
del framework di assurance del tutto simile a quella di qualsiasi altro container usato
in una pipeline. I requisiti sono i seguenti, divisi tra \emph{obbligatori}, cioè requisiti che \emph{devono}
essere soddisfatti, e requisiti \emph{non obbligatori}, cioè requisiti che, sebbene non obbligatori,
sarebbe meglio che fossero rispettati.

\noindent \textbf{Requisiti obbligatori} I requisiti obbligatori sono definiti come segue.

\begin{description}
    \item[Configurazione con variabili d'ambiente] I parametri che dettano l'esecuzione
    dell'attività di assurance eseguite dal \clientcontainer\ devono poter essere configurati
    mediante variabili d'ambiente.
    \item[Astrazione] I dettagli dell'esecuzione devono essere astratti e nascosti al team DevOps.
    Non è necessario che il team DevOps conosca i dettagli di basso livello necessari
    alla configurazione di un'attività di verifica del framework.
\end{description}

\noindent \textbf{Requisiti non obbligatori} I requisiti non obbligatori sono definiti come segue.

\begin{description}
    \item[Configurazione minimale] Il numero di configurazioni che il team DevOps deve specificare,
    per avere il \clientcontainer\ funzionante, dovrebbe essere ridotto al minimo possibile.
    Il \clientcontainer\ dovrebbe essere quindi dotato di valori di default ragionevoli, comunque
    sovrascrivibili dal team DevOps.
    \item[Immagine minimale] L'immagine del \clientcontainer\ dovrebbe seguire il principio della
    minimizzazione delle dimensioni. Tendenzialmente, infatti, si cerca di avere immagini che
    abbiano una dimensione il minore possibile, per limitare i requisiti di memoria
    dell'host su cui devono essere memorizzate e successivamente eseguite.
\end{description}

L'utilizzo di un container eseguito localmente nella pipeline fa emergere nuovi pattern interessanti ed
evoluti, espressi nel requisito non obbligatorio di \emph{certificazione distribuita}.
Inoltre, esso consente di colmare una delle lacune indicate in
Sezione~\ref{sec:methodology-why-assurance-cannot-be-used-as-is}, cioè \emph{security as code}, grazie
al file di configurazione della pipeline fornito con il container.

\subsubsection{Controls-triggered continuous certification}\label{subsubsec:methodology-framework-requirements-controls-triggered-continuous-certification}
La necessità di dover ri-effettuare la fase di certificazione in seguito al cambiamento negli
strumenti di verifica è stata espressa nei requisiti del processo come \emph{certificazione continua e trigger esterni}.
La concretizzazione di tale requisito, nel framework che implementa il processo, si realizza
negli aggiornamenti ai \emph{controlli}. Infatti, ogni volta che un controllo del framework di certificazione
viene aggiornato, tutte le pipeline che lo utilizzano devono ripartire, ri-effettuando la certificazione
del processo di sviluppo. Come detto nei requisiti del processo, nel farlo è possibile sfruttare
il concetto di \emph{influenza}.

Unendo questo requisito con quello di \trustcontrols, il processo di certificazione deve
ripartire ogni volta che vi è stato un aggiornamento in uno dei controlli usati, sapendo che
tali controlli sono \emph{trustworthy}, e che l'aggiornamento è avvenuto in seguito ad una rigorosa
fase di testing.

\subsubsection{Altri requisiti}\label{subsubsec:devsecops-framework-requirements-other-requirements}
\noindent \textbf{Certificazione distribuita} I vari \gitlabjob\ di una pipeline hanno accesso
al codice della \emph{repository}, e ad eventuali artefatti prodotti dai \gitlabjob\ precedenti.
Il \clientcontainer\ dovrebbe sfruttare questo, e non limitarsi a chiamare le API del
framework di assurance per eseguire la certificazione. Il \clientcontainer\ potrebbe effettuare
parte delle computazione localmente, sfruttando proprio l'avere accesso al codice ed
agli artefatti. Una volta effettuate tutte la attività possibili localmente, il \clientcontainer\ dovrebbe
inviarne i risultati al framework di assurance, e delegarne ad esso l'analisi degli stessi, per decidere se
rilasciare o meno il certificato. 
Un paradigma di questo tipo porta due vantaggi principali. In primo luogo, alleggerisce il carico di lavoro
del framework di assurance. In secondo luogo, diminuisce il traffico tra la pipeline ed il framework
di assurance, poiché il \clientcontainer\ invia al framework il risultato di una prima computazione
sugli artefatti, anziché gli artefatti stessi, che possono essere eventualmente voluminosi.
D'altro canto, richiede maggiore complessità implementativa per il framework di assurance,
perché deve essere in grado di gestire in input sia artefatti, sia il risultato di una prima computazione
su di essi.

\begin{table}[t]
    \centering
    \caption[Riassunto dei requisiti del \emph{framework di assurance}]{Riassunto dei requisiti del \emph{framework di assurance}.}
    \label{tbl:req-summary-framework}
    %\resizebox{1.\textwidth}{!}{
    \begin{tabular}{|p{.4\textwidth}|p{.23\textwidth}|p{.24\textwidth}|}
        \hline
        \multirow{2}{*}{Nome requisito} &
        \multicolumn{2}{c|}{Scopo} \\\cline{2-3}
        & Allineamento tra metodologia e implementazione & Inserimento certificazione in pipeline CI/CD\\
        \hline
        \hline
        Evidence-based collection & \yes & \no \\
        \hline
        \emph{Trustworthy} controls & \yes & \no \\
        \hline
        Scalabilità & \no & \almost \\
        \hline
        API-driven & \no & \yes \\
        \hline
        CI/CD-native & \no & \yes \\
        \hline
        Controls-triggered continuous verification & \almost & \almost \\
        \hline
        Certificazione distribuita & \no & \yes \\
        \hline 
    \end{tabular}
    %}
\end{table}
