\section{Pipeline con trigger esterni}\label{sec:case-study-controls-triggered}
%\section{Controls-triggered continuous certification}\label{sec:case-study-controls-triggered}

Nella Sezione~\ref{sec:methodology-requirements} inerente ai requisiti, si è ribadita la necessità
di poter eseguire una pipeline CI/CD anche in risposta a cambiamenti nel contesto
della sicurezza, in concreto in aggiornamenti nei controlli di Moon Cloud utilizzati
(Capitolo~\ref{cert-controls-triggered}). Questo requisito
è stato chiamato \certcontrolstrigg.
Moon Cloud offre il componente \restgistry, che espone una API REST ritornante il \emph{digest}
delle immagini Docker dei controlli usati in una certa \aer. Tale componente è realizzato
secondo il paradigma di \notification. Per soddisfare il
requisito \certcontrolstrigg\ occorre anche del codice lato client, che interroghi \restgistry\ e
invochi nuovamente la pipeline, in caso di aggiornamenti. L'onere delle realizzazione
del codice lato client è sulle spalle del team DevOps.

Nel caso specifico, il team DevOps deve realizzare un piccolo script per verificare
cambiamenti nel controllo componente l'\aer\ \aerrustcheck.

% Nel caso specifico, il team DevOps responsabile dello sviluppo della probe \probeservicetime\ 
% utilizza l'\aer\ \aerrustcheck, per la scansione delle dipendenze del codice Rust.
% Per poter orchestrare l'esecuzione della propria pipeline anche in risposta a cambiamenti esterni,
% devono realizzare un piccolo script lato client.

Lo script realizzato è stato scritto anch'esso in Rust, e funziona in maniera simile all'esempio
mostrato in Sezione~\ref{sec:cert-controls-triggered-client}. Esso è un programma \emph{stand-alone},
che utilizza le API di Gitlab (il framework usato per lo sviluppo dalla probe \probeservicetime) per
lanciare l'esecuzione della pipeline. Gitlab, infatti, fornisce una API HTTP che, previa autenticazione,
può essere chiamata per far partire l'esecuzione di una certa pipeline.
Lo script memorizza su un file di testo il \emph{digest} ottenuto al momento dell'ultima esecuzione, e
lo confronta con quello ottenuto interrogando \restgistry.
Nel dettaglio, il client funziona come segue.
Come prima cosa, ottiene il nuovo \emph{digest} interrogando \restgistry, e lo confronta
con il \emph{digest} dell'ultima esecuzione. Se vi è una differenza, allora viene fatta una
chiamata alla API di Gitlab, e la pipeline viene eseguita di nuovo. Infine, il file
contenente il \emph{digest} viene aggiornato con il nuovo \emph{digest}.
Lo stesso file viene mantenuto aggiornato da un secondo script, eseguito da un \emph{Git hook}
quando si effettua un \emph{push} del codice di \probeservicetime\ verso la \emph{repository} Gitlab~\cite{git-hooks}.
Questo secondo script è necessario per ottenere il valore del \emph{digest} quando la pipeline
parte, innescata da un \emph{push}, cioè da delle modifiche nel codice.

Una versione semplificata del client Rust viene mostrata di seguito.
\inputminted[firstline=27, lastline=125, highlightlines={52-57}]{rust}{code/09_04_restgistry_sample_client.rs}

% Per poter lanciare l'esecuzione di una pipeline mediante API HTTP con Gitlab, occorre specificare
% un token di autenticazione, configurabile nelle impostazione della \emph{repository}.
Il client viene eseguito con le seguenti opzioni.

\begin{itemize}
    \item \cmd{--aer-id} Specifica l'ID dell'\aer\ di cui si vuole verificare gli aggiornamenti.
    \item \cmd{--branch-name} Specifica il nome del \emph{branch} di Git presso cui si vuole invocare
    la pipeline.
    \item \cmd{--digest-file} Specifica il percorso per il file contenente i \emph{digest}.
    \item \cmd{--gitlab-api-url} Specifica la URL della API HTTP di Gitlab da invocare per far
    partire la pipeline.
    \item \cmd{--gitlab-token} Specifica il token di autenticazione per Gitlab. Possedere un token
    è necessario per invocare l'API che fa partire una pipeline. Il token viene rilasciato
    da Gitlab mediante un'apposita sezione della sua interfaccia grafica.
    \item \cmd{--mooncloud-api-url} Specifica la URL della API REST di \restgistry\footnote{Si noti che,
    a fini dimostrativi, tale API è non autenticata.}.
\end{itemize}

Per poter funzionare in maniera periodica, l'esecuzione del client viene schedulata dal team DevOps
mediante un \emph{cronjob}, con un intervallo a piacere, nel caso in questione,
1 giorno. Può sembrare un intervallo elevato, tuttavia tale scelta viene fatta dal team DevOps
sulla base della frequenza con cui le probe di Moon Cloud vengono aggiornate. In particolare,
considerata la stabilità della probe in questione, cioè \aerrustcheck, 1 giorno è un intervallo
persino ottimistico.

In Figura~\ref{fig:integration-cert-controls-triggered} si mostra un esempio di esecuzione del client nel caso in cui
viene rilevato un aggiornamento. Nel caso in cui non vi siano aggiornamenti, il client
semplicemente stampa \cmd{No difference evidenced}.

\begin{figure}[!t]
    \centering
    \includegraphics[scale=0.5]{img/09_04_integration_controls_triggered_exec.png}
    \caption[Esempio di esecuzione di un client di \restgistry]{Esempio di esecuzione del client di \restgistry: l'output è un estratto della risposta
    ritornata dalla API HTTP di Gitlab.}
    \label{fig:integration-cert-controls-triggered}
\end{figure}

\noindent \textbf{Discussione} Per concludere, il soddisfacimento del requisito \certcontrolstrigg\
consiste in due componenti, \restgistry, implementato in Moon Cloud, ed un client che lo interroghi,
il quale, se rileva aggiornamenti, fa partire la pipeline. Poiché il team DevOps utilizza Gitlab, che
supporta l'esecuzione di pipeline mediante API HTTP, è stato sufficiente realizzare un client
che interroghi \restgistry\  ed in seguito la API di Gitlab. Il paradigma di \notification,
implementato lato server da \restgistry, sposta parte della complessità sul team DevOps richiedendo
la realizzazione di un client. Il caso di studio in questione mostra come questa complessità sia
bassa, e come, mediante un semplice programma schedulabile con \emph{cron}, sia possibile
ottenere il requisito di \certcontrolstrigg, garantendo che la pipeline venga ri-eseguita se
si verificano degli aggiornamenti nei controlli in essa usati.
