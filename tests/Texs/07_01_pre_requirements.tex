\section{Prerequisiti}\label{sec:trustworthy-pre-req}

\subsection{\dockercompose}\label{subsec:trustworthy-pre-req-docker-compose}
\dockercompose\ è un semplice strumento di orchestrazione per container Docker~\cite{docker-compose}.
Consente di definire, in maniera dichiarativa in un file YAML, un insieme di container che è possibile gestire
in maniera uniforme. Da un punto di vista logica, i container definiti nel file appartengono
alla stessa applicazione.
Nei fatti, \dockercompose\ è un semplice layer di astrazione al di sopra di Docker stesso; fornisce dei
comandi per la gestione dello \emph{stack} applicativo definito nel file di configurazione.
Tale file ha un formato ben preciso~\cite{docker-compose-file-ref}, il cui nome di default è
\dockercomposefile\ (è possibile anche che l'estensione sia \filename{.yaml}).

\dockercompose\ può essere usato per descrivere il \emph{deployment} di un'applicazione. È infatti naturale che
nel mondo dei microservizi un'applicazione sia composta da numerose parti, e ciascuna di essa è un container.
Sfruttando il file \dockercomposefile\ è possibile definire tutti i parametri per il \emph{deployment} di tale
applicazione, specificando quali sono i container da eseguire e loro configurazioni specifiche, comprese
di variabili d'ambiente e porte esposte. Si può anche configurare il networking virtuale, specificando
la rete virtuale da creare, quale deve essere la sua subnet, e quali indirizzi IP assegnare a ciascun container.
Un altro caso d'uso per cui \dockercompose\ viene suggerito è quello del testing. È possibile infatti
definire nel file \dockercomposefile\ tutti i container necessari e creare un ambiente di test completamente
riproducibile, che può essere creato e distrutto sulla base delle necessità.

L'amministrazione di uno \emph{stack} \dockercompose\ è molto semplice, e si basa sul comando \cmd{docker-compose}.
I due comandi principali sono \dockercomposeup\ e \dockercomposedown, che funzionano come segue.

\begin{description}
    \item[\dockercomposeup] Eseguire questo comando, all'intero di una directory in cui è presente
    un file chiamato \dockercomposefile, ha l'effetto di creare tutte le immagini dei container indicati
    nel file, se non sono già presenti, ed eseguire ogni container usando le opzioni specificate.
    È possibile specificare il percorso al file di orchestrazione, nel caso in cui abbia un nome diverso
    da quelli standard, o si trovi un una directory diversa da quella in cui il comando viene lanciato.
    \item[\dockercomposedown] Eseguire questo comando, all'intero di una directory in cui è presente
    un file chiamato \dockercomposefile, ha l'effetto di fermare l'esecuzione di tutti i container
    indicati, nel caso in cui siano in esecuzione. Anche in questo caso, è possibile specificare il percorso
    al file di orchestrazione se necessario.
\end{description}

Un altro comando utile è \cmd{docker-compose build}, il quale è in grado di effettuare la \emph{build} di
tutte le immagini dichiarate nel file \dockercomposefile.

Dietro le quinte, \dockercompose\ funziona sfruttando i comandi \cmd{docker}, 
\dockercompose\ consente però una maggiore automazione.
Si consideri l'esempio seguente.

\inputminted[breaklines, linenos, fontsize=\footnotesize]{yaml}{code/05_01_docker-compose_example.yml}

Si definiscono, sotto la chiave \key{services}, i servizi, ovvero i container, che definiscono
lo \emph{stack} applicativo. I due servizi sono chiamati \key{db} e \key{api} (\codelines{6}{19} e \codelines{22}{38}
rispettivamente).
Per ciascuno, usando la chiave \key{networks}, si definisce la rete virtuale di appartenenza (\codelines{7}{8} e \codelines{23}{24}).
Le specifiche di tale rete, denominata \key{sample\_net}, vengono dichiarate tra le righe \lineno{41} e
\lineno{43}.
Per il servizio \key{api}, si specificano anche le informazioni relative a come fare la \emph{build} dell'immagine
utilizzata, sfruttando la chiave \key{build} (\codeline{25}). In questo caso il valore \key{.} è uno \emph{shortcut}, indicante che
come directory di base per effettuare la \emph{build} va usata quella corrente, e che il file di riferimento per
descrivere la \emph{build} si chiama \filename{Dockerfile}.
Inoltre, sfruttando la chiave \key{environment}, è possibile specificare un insieme di variabili d'ambiente
che verranno passate al container durante l'esecuzione (\codelines{10}{12} e \codelines{29}{36}).

Per effettuare la \emph{build} di tutte le immagini indicate nel file, che una volta in esecuzioni diventeranno dei container,
è sufficiente un comando: \cmd{docker-compose build}. Successivamente, mediante il comando
\cmd{docker-compose up -d} si fanno partire tutte le immagini, in modalità \emph{detached}, ovvero
in background. Per fermarne l'esecuzione è sufficiente il comando \dockercomposedown.

Se invece \dockercompose\ non fosse disponibile, occorrerebbe eseguire il comando \cmd{docker build},
per effettuare le \emph{build} delle immagini, per ciascuna immagine. Poi, occorrerebbe eseguire
il comando per la creazione della rete virtuale. In seguito, occorrerebbe eseguire i comandi \cmd{docker run},
per eseguire ogni immagine, facendola diventare un container. In seguito ancora,
occorrerebbe eseguire i comandi che collegano i container alla rete virtuale.

Per concludere, mediante \dockercompose, è possibile con pochi semplici comandi amministrare uno \emph{stack}
applicativo arbitrariamente complesso. I comandi mostrati, infatti, valgono per l'esempio composto
da due container, ma anche per uno \emph{stack} composto da decine di container. \dockercompose\ consente
quindi un grande risparmio di tempo, evitando di dover digitare i comandi di \emph{build}, esecuzione, configurazione
della rete e stop dell'esecuzione per ciascun container.

\subsection{Rust}\label{subsec:trustworthy-pre-req-rust}
Rust è un linguaggio di programmazione sviluppato da Mozilla, con enfasi su sicurezza, performance
e manutenibilità~\cite{rust, rust-debian}.
Tra le sue caratteristiche peculiari si possono citare le seguenti.
\begin{itemize}
    \item \emph{Memory safety senza garbage collection}, garantita a tempo di compilazione. Il compilatore,
    grazie alle sue capacità di analisi statica, rigetta tutto il codice che non è \emph{memory-safe},
    evitando per definizione bug come \emph{use-after-free}.
    \item \emph{Thread-safety}, garantita a tempo di compilazione, grazie al modello di memoria usato da Rust.
    In particolare, non sono possibili \emph{data race}.
    \item \emph{Elevate performance}, grazie al backend di compilazione \emph{LLVM}~\cite{llvm} ed alla quasi
    assenza di un runtime.
    \item \emph{Tipizzazione forte} e \emph{type-inference}, per cui il programmatore è obbligato a specificare
    i tipi nella signature delle funzioni, mentre i tipi delle variabili sono dedotti dal compilatore.
    \item \emph{Ampio uso di interfacce} e \emph{generici}. In Rust non vi è il concetto di classe,
    semmai si possono definire delle \emph{struct} con dei \emph{metodi} associati; è possibile
    implementare alcuni paradigmi della programmazione ad oggetti sfruttando però le interfacce (dette \emph{trait})
    ed i generici.
\end{itemize}

La popolarità di Rust aumenta costantemente, dal 2016 esso è il \emph{linguaggio di programmazione preferito}
nella \emph{survey} annuale di \emph{StackOverflow}~\cite{stackoverflow-2016, stackoverflow-2017, stackoverflow-2018, stackoverflow-2019}.
È usato in numerosi progetti, da strumenti per il cloud computing (ad esempio~\cite{firecracker, bottlerocket, wired-rust}),
a proxy (ad esempio~\cite{linkerd}), a database distribuiti (ad esempio~\cite{tikv}).

\noindent \textbf{Perché Rust} Il linguaggio è stato scelto per l'implementazione del requisito \trustcontrols.
L'intera infrastruttura Moon Cloud è basata su Python, un linguaggio di programmazione che sta agli
antipodi rispetto a Rust, caratterizzato da un'enfasi sulla dinamicità piuttosto che sulla \emph{safety} e sulle
performance. Scegliere Rust è stata una decisione precisa, dettata dalla frustrazione di lavorare con
un linguaggio come Python. Questa frustrazione è ben documentata, ed è uno dei motivi che ha portato ad aggiungere
l'annotazione dei tipi a Python, e la creazione di ben due linguaggi per sopperire alla mancanze di Javascript
(\emph{Dart}~\cite{dart} e \emph{TypeScript}~\cite{typescript}, l'ultimo dei quali ha avuto molto successo~\cite{typescript-friends}).
Si è scelto di provare ad usare e un linguaggio di programmazione che per impostazione fosse completamente
diverso da Python. La realizzazione del requisito \trustcontrols, essendo un progetto relativamente piccolo,
è stato il banco di prova perfetto.

\subsection{Pipeline senza testing}\label{subsec:trustworthy-pre-req-old-pipeline}

Tutti i controlli di Moon Cloud utilizzano la stessa pipeline, quindi lo stesso file
\gitlabci. Grazie ad un sapiente uso di variabili d'ambiente, i file sono standard ed uguali per ogni controllo.
Tale pipeline esegue concettualmente due passi. Il primo passo consiste nel fare una \emph{build}
dell'immagine specificando come \dockertag\ uno diverso da quello dell'immagine di produzione
memorizzata nel \moonccomp{Model DB}\footnote{Si ricorda che il nome di un'immagine Docker è composto
dall'URL del \emph{registry}, dal nome effettivo dell'immagine, e da un \dockertag\ indicante la
versione dell'immagine.}. Il passo successivo consiste semplicemente nel taggare
l'immagine appena costruita con il \dockertag\ dell'immagine di produzione.
La pipeline utilizzata è la seguente.

\inputminted{yaml}{code/05_01_old_probe_ci.yml}

\begin{description}
    \item[\emph{Variables}] Vengono due definite due variabili che specificano il
    nome dell'immagine di test (\codeline{2}) e dell'immagine di produzione (\codeline{3}).
    Nella definizione di queste variabili si sfruttano altre variabili definite da Gitlab stesso,
    in particolare \var{CI\_REGISTRY\_IMAGE}, \var{CI\_COMMIT\_REF\_NAME}. Esse corrispondo
    al nome \emph{di base} dell'immagine\footnote{Il nome \emph{di base} dell'immagine viene
    definito automaticamente da Gitlab sulla base dell'URL del progetto cui
    si riferisce all'interno della \emph{repository} Git.} e il nome del Git \emph{branch} o
    Git \emph{tag} da cui la \emph{build} parte, rispettivamente~\cite{gitlab-predefined-variables}.
    \item[\emph{Stage}] I controlli utilizzano una pipeline a due \gitlabstage\ personalizzati:
    \emph{build} e \emph{release}~(\codelines{5}{7}).
    \item[\emph{Before script}] Prima di ogni \gitlabjob\ si esegue quanto contenuto
    nella chiave \gitlabbeforescriptt\ (\codelines{11}{13}). In particolare
    si fa login presso il \emph{registry}, usando come utente \emph{gitlab-ci-token}, un utente
    predefinito da Gitlab. 
    \item[\emph{Job}] La pipeline è composta da due soli \gitlabjob, con lo stesso nome degli
    \gitlabstage.
    Il \gitlabjob\ di \emph{build}~(\codelines{16}{24}) esegue la \emph{build} dell'immagine
    Docker del controllo, usando il comando \cmd{docker build}~(\codeline{21}). Questa \emph{build} crea
    l'immagine di test.
    Successivamente, tale immagine viene caricata nel \emph{registry}, e si stampa su \emph{console}
    il nome dell'immagine, salvato nella variabile \var{CI\_REGISTRY} (\codeline{23} e
    \codeline{24}, rispettivamente).
    Il \gitlabjob\ di \emph{release}~(\codelines{27}{38}) scarica localmente l'immagine di test
    dal \emph{registry} (\codeline{32}), la \emph{tagga} usando il \emph{tag} di produzione~(\codeline{34})
    e pubblica la nuova immagine con il tag di produzione sul \emph{registry}~(\codeline{36}).
    Questo \gitlabjob\ è eseguito solo per il \emph{branch master}.
\end{description}

Si noti che in questa pipeline non si specifica quale immagine Docker va utilizzata per
l'esecuzione: nel caso in cui questa configurazione manchi si utilizza quella
di default prestabilita da Gitlab.
