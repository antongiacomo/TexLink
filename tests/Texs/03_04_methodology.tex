\section{Metodologia}\label{sec:methodology-methodology}
Il nuovo modello di certificazione considera due dimensioni che influiscono sulla proprietà
che viene certificata. La prima dimensione è il processo di sviluppo, oggetto di verifica. I concetti
di \emph{proprietà} ed \emph{evidenze} sono relativi a questa dimensione.
La seconda dimensione, invece, riguarda il processo di verifica stesso. Infatti, anche
\emph{come} una proprietà viene certificata è importante. Il fatto che per la verifica si utilizzi
un framework che dà certe garanzie, piuttosto che altre, influisce sulla proprietà finale.
Un confronto tra che cosa la nuova metodologia offre, rispetto a quelle attuali, è mostrato in Figura~\ref{fig:methodology-comparison}.

\begin{figure}[t]
    \centering
    \includegraphics{img/03_04_methodology.pdf}
    \caption[Confronto tra il nuovo schema di certificazione presentato e gli schemi esistenti]{Confronto tra il nuovo schema di certificazione presentato e gli schemi esistenti.}
    \label{fig:methodology-comparison}
\end{figure}

\subsection{Processo di sviluppo}\label{subsec:methodology-methodology-process-development}
Il processo di sviluppo è un procedimento complesso,
composto da diverse fasi, tant'è che esistono molte metodologie diverse, dalle più classiche
\emph{waterfall}, alle metodologie \emph{agili}. Le scelte fatte durante questo processo
si riflettono nel prodotto finale, e scelte diverse possono portare ad avere due software che,
per certe proprietà non funzionali, sono uguali. Viceversa, due software che svolgono esattamente
le stesse funzionalità, possono essere molto diversi dal punto di vista di alcune proprietà non funzionali
(si veda la Sezione precedente). Il processo di certificazione deve riflettere questa differenza,
mediante un'attenta modellazione dei concetti di \emph{proprietà} ed \emph{evidenze}.

% \begin{definition}[Valori]\label{def:methodology-value}
%     L'insieme di 
% \end{definition}

\begin{definition}[Proprietà non funzionale]\label{def:methodology-property}
    Una proprietà non funzionale $p$ è una coppia $(p, a_{A_p})$, con $p$ indicante il nome delle proprietà,
    $a_{A_p} \in A_p$ il valore della proprietà, ed $A_p$ indicante l'insieme di valori possibili.
    La relazione di ordinamento totale su $A_p$ viene indicata con $\preceq_{A_p}$.
\end{definition}

\begin{definition}[Proprietà non funzionale vuota]\label{def:methodology-property-empty}
    Una proprietà non funzionale $p=(p, a_{A_p})$ è detta \emph{vuota} sse $A_p = \emptyset$.
\end{definition}

\begin{definition}[Confronto tra proprietà]\label{def:methodology-property-compare}
    Siano $p_i, p_j$ due proprietà con $p_i.p = p_j.p$, $p_i$ è detta \emph{migliore rispetto} a $p_j$
    sse $p_j \preceq_{A_p} p_i$.
    È espresso in simboli con $p_i \succeq_{P} p_j$.  
\end{definition}

\begin{definition}[Confroto tra proprietà vuote]\label{def:methodology-property-compare-empty}
    Siano $p_i, p_j$ due proprietà con $p_i.p = p_j.p$ e $p_i.a_{A_p} = p_j.a_{A_p} = \emptyset$,
    allora vale $p_i \succeq_{P} p_j \land p_j \succeq_{P} p_i$.
\end{definition}

Si noti che una proprietà non funzionale vuota è semplicemente una proprietà di cui non si vuole certificare
il valore, ma l'esistenza.

\begin{definition}[Evidenza]\label{def:methodology-evidence}
    Una evidenza $ev$ è un elemento dell'insieme $E_p$, definito come l'insieme delle evidenze ammissibili
    per supportare una certa proprietà $p$.
    La relazione di ordinamento totale su $E_p$ viene indicata con $\preceq_{E_p}$.
\end{definition}

\begin{definition}[Confronto tra evidenze]\label{def:methodology-evidence-compare}
    Siano $ev_i, ev_j$ due evidenze $\in E_p$, $ev_i$ è detta \emph{evidenza migliore a supporto di $p$ rispetto} a $ev_j$
    sse $ev_j \preceq_{E_p} ev_i$.
    È espresso in simboli con $ev_i \succeq_{B_E} ev_j$.
\end{definition}

\subsection{Processo  di verifica}\label{subsec:methodology-methodology-process-certification}
Il processo di certificazione, basato sui concetti di proprietà e evidenze appena descritti,
viene concretizzato da un framework di assurance, e vi possono essere vari framework che
implementano lo stesso processo in modo diverso. Le garanzie che un framework
fornisce rispetto ad un altro possono essere diverse. Di particolare importanza sono i
\emph{controlli}, intesi come gli strumenti che il framework utilizza per verificare la presenza, o meno,
di evidenze. Un framework, infatti, potrebbe utilizzare dei controlli testati e verificati in maniera
rigorosa. Un altro framework, invece, potrebbe sfruttare dei controlli realizzati in maniera poco rigorosa,
addirittura malfunzionanti in certi casi. In altre parole, il processo di \emph{verifica} delle proprietà
può essere realizzato in molti modi diversi, alcuni dei quali più validi di altri.
Tutto ciò si riflette sulle proprietà che vengono certificate;
a tale scopo, si definisce il concetto di \emph{metodologia} come segue.

\begin{definition}[Attributi descrittivi]\label{def:methodology-methodology-attribute}
    Siano $m_1, m_2, \cdots, m_n$ degli attributi usabili per descrivere come avviene
    un processo di verifica. L'insieme $M=\{m_1, m_2, \cdots, m_n\}$ è detto \emph{insieme degli attributi descrittivi}.
    La relazione di ordinamento totale definita su $M$ è indicata con $\preceq_M$.
\end{definition}

\begin{definition}[Metodologia]\label{def:methodology-methodology}
    Sia $M$ l'\emph{insieme degli attributi descrittivi}, una \emph{metodologia} $cm$ è un
    sottoinsieme di $M^\ast$.
\end{definition}

In altre parole, una metodologia $cm$ è un insieme di attributi che descrivono il processo
di verifica.

\begin{definition}[Confronto tra metodologie]\label{def:methodology-compare}
    Siano $cm_i=\{m_1, m_2, \cdots, m_m\}$ e $cm_j=\{m_1, m_2, \cdots, m_m\}$ due metodologie, $cm_i$ è detta \emph{migliore} di $cm_j$,
    se  $\forall m_k\ \in cm_j, m_l\ \in cm_i\ m_k\ \preceq_M\ m_l$.
    È espresso in simboli con $cm_i \succeq_{B_M} cm_j$.
\end{definition}

In altre parole, una metodologia $cm_i$ è migliore di una metodologia $cm_j$ se ogni
elemento di $cm_i$ è non minore di ogni elemento di $cm_j$, secondo la relazione $\preceq_M$.

\subsection{Certificati}\label{subsec:methodology:methodology-process-certificates}

\begin{definition}[\certmodelt]\label{def:methodology-certmodelt}
    Un \certmodelt\ $t$ è definito dalla tripla $(p, S, f)$, composta come segue.
    \begin{itemize}
        \item $p$ indica la proprietà oggetto di certificazione.
        \item $S$ indica l'insieme delle evidenze $ev \in E$ necessarie a supportare la proprietà $p$.
        Per semplicità, si assume che sia sufficiente un elemento qualsiasi dell'insieme $S$ per supportare $p$.
        \item $f$ indica la firma sul \certmodelt\ rilasciata da una \emph{certification authority}.
    \end{itemize}
\end{definition}

\begin{definition}[\certmodeli]\label{def:methodology-certmodeli}
    Un \certmodeli\ $i$ è l'istanziazione di un \certmodelt\ in uno specifico target di certificazione,
    definito dalla tripla $(\overline{p}, \overline{S}, t)$, composta come segue.
    \begin{itemize}
        \item $\overline{p}$ indica la proprietà certificata.
        \item $\overline{S}$ indica l'istanziazione dell'insieme $S$ delle evidenze a supporto di $\overline{p}$.
        Uno qualsiasi degli elementi di $\overline{S}$ è sufficiente per supportare $\overline{p}$.
        \item $t$ indica il \certmodelt\ di cui $i$ è l'istanziazione.
    \end{itemize}
\end{definition}

\begin{definition}[Certificato]\label{def:methodology-cert}
    Un certificato $c$ è definito dalla tupla
    $(\overline{p}, \overline{ev}, m, i)$, composta come segue.
    \begin{itemize}
        \item $\overline{p}$ indica la proprietà certificata.
        \item $\overline{ev}$ indica l'evidenza raccolta a supporto di $\overline{p}$. Nel caso in cui, nel target
        di certificazione, siano disponibili più evidenze $\in E_p$, si raccoglie solo la migliore, secondo la relazione
        $\succeq_{B_E}$.
        \item $m$ descrive la metodologia di certificazione usata.
        \item $i$ rappresenta il \certmodeli\ istanziato per il certificato in questione.
    \end{itemize}
\end{definition}

Sulla base delle varie definizioni, è infine possibile definire come \emph{confrontare} due certificati.

\begin{definition}[Confronto tra certificati]\label{def:methodology-cert-compare}
    Siano $c_i, c_j$ due certificati con $c_i.\overline{p}=c_j.\overline{p}$, $c_i$ è detto \emph{migliore} di $c_j$ sse
    $c_i.\overline{p.A_p} \succeq_{P} c_j.\overline{p.A_p}\ \land\ c_i.\overline{ev} \succeq_{B_E} c_j.\overline{ev}\ \land\ c_i.m \succeq_{B_M} c_j.m$.
\end{definition}

In altre parole, dati due certificati attestanti una stessa proprietà, uno dei due è migliore
dell'altro se il valore della proprietà è \emph{migliore}, l'evidenza usata è \emph{migliore}
e la metodologia usata è \emph{migliore}, in accordo alle definizioni di \emph{migliore} per ciascuno.

\subsection{Applicazione nello scenario di riferimento}\label{subsec:methodology-example}
Si supponga di voler applicare la nuova metodologia appena descritta allo scenario di riferimento
(Sezione~\ref{sec:methodology:motivation}), in cui bisogna certificare il processo di sviluppo
seguito dai due team, il team C++ ed il team Rust. Per farlo, si procede come segue.

\begin{description}
    \item[Proprietà] Le proprietà di interesse sono \emph{memory safety} e \emph{assenza di data race}, definite
    come segue.
    \begin{itemize}
        \item \emph{memory safety}: $(ms,), A_{ms}=\emptyset$
        \item \emph{assenza data race}: $(adr, ), A_{adr}=\emptyset$
    \end{itemize} 
    \item[Evidenze] Le evidenze sono definite segue, ordinate secondo la relazione $\succeq_{B_E}$.
    \begin{itemize}
        \item $E_{ms} = \{$\emph{testing non funzionale per memory safety, garanzie formali}$\}$
        \item $E_{adr} = \{$\emph{testing non funzionale per assenza data race, garanzie formali}$\}$
    \end{itemize}
    \item[Metodologie] Si definiscono due possibili metodologie come segue, a partire dall'insieme di attributi
        $M=\{$\emph{non trustworthy}, \emph{trustworthy}$\}$.
        \begin{itemize}
            \item $m_{nt} = \{$\emph{non trustworthy}$\}$
            \item $m_t = \{$\emph{trustworthy}$\}$
        \end{itemize}
    \item[\certmodelt] Il \certmodelt\ che si usa per la certificazione è definito come segue, uno
        per ciascuna proprietà.
        \begin{itemize}
            \item $t_{ms} = (ms, E_{ms}, \mathtt{<signature>})$
            \item $t_{adr} = (adr, E_{adr}, \mathtt{<signature>})$
        \end{itemize} 
    \item[\certmodeli] I due \certmodelt\ vengono istanziati come segue. In questo caso, l'istanziazione
        non aggiunge informazioni particolari rispetto ai \certmodelt\ (Sezione~\ref{sec:methodology:motivation}).
    \begin{itemize}
        \item $i_{ms} = (\overline{ms}, \overline{E_{ms}}, t_{ms})$
        \item $i_{adr} = (\overline{adr}, \overline{E_{adr}}, t_{adr})$
    \end{itemize} 
    \item[Certificati] Infine, si rilasciano quattro certificati, uno per ciascuna proprietà a ciascun team. Si suppone
    che nella certificazione sia stata usata la metodologia $m_t$. 
    \begin{itemize}
        \item Team C++, proprietà \emph{memory safety}: $c_1 = (\overline{ms},\ $\emph{testing non funzionale per memory safety}$,$ $m_t,$ $i_{ms})$
        \item Team C++, proprietà \emph{assenza data race}: $c_2 = (\overline{adr},\ $\emph{testing non funzionale per assenza data race}$,$ $m_t,$ $i_{adr})$
        \item Team Rust, proprietà \emph{memory safety}: $c_3 = (\overline{ms},\ $\emph{garanzie formali}$,$ $m_t,$ $i_{ms})$
        \item Team Rust, proprietà \emph{assenza data race}: $c_4 = (\overline{adr},\ $\emph{garanzie formali}$,$ $m_t,$ $i_{adr}$ $)$
    \end{itemize}   
\end{description}

Sulla base dei certificati rilasciati al team C++ ($c_1, c_2$) ed al team Rust ($c_3, c_4$), è possibile
confrontare il processo di sviluppo seguito, espresso nelle due proprietà di interesse \emph{memory safety} e 
\emph{assenza data race}. Applicando la definizione di \emph{confronto tra certificati} (Definizione~\ref{def:methodology-cert-compare}),
si possono confrontare solo certificati con una stessa proprietà.
Confrontando $c_1$ e $c_3$, essendo $\overline{p.A_p}$ vuoto, essendo le due metodologie $m$ usate uguali,
l'unica differenza è nelle evidenze. Infatti, $c_1$ utilizza l'evidenza \emph{testing non funzionale} per
la proprietà \emph{memory safety}, mentre $c_3$ utilizza \emph{garanzie formali}.
Applicando la definizione di \emph{confronto tra evidenze} (Definizione~\ref{def:methodology-evidence-compare}),
emerge che la migliore è \emph{garanzie formali}, quindi $c_3$ è un certificato migliore di $c_1$.
Lo stesso discorso vale anche per il confronto tra $c_2$ e $c_4$.
Quindi, i certificati rilasciati al team Rust sono migliori, e ciò riflette la realtà, un risultato che
non era precedentemente possibile.
