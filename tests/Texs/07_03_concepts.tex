\section{Concetti}\label{sec:trustworthy-concepts}
L'esecuzione della fase di testing si basa sui seguenti tre concetti: \testenv, \testcase\ e \testexec.
In sostanza, per ciascun controllo si definiscono una serie di \testcase, che insieme
contribuiscono a definire il \testenv\ di quel controllo. Tale ambiente costituisce un target
per l'esecuzione del controllo, per il quale si conoscono esattamente gli input da passare
al controllo per la sua esecuzione, e gli output che ci si aspetta.
Il \testexec\ è il componente della pipeline che si occupa dell'effettiva esecuzione dei controlli
verso tali \testenv, verificando che gli input ritornati dal controllo siano uguali
a quelli che ci si aspetta.

\subsection{\testenv\ e \testcase}\label{subsec:trustworthy-concepts-testing-testenv}

\subsubsection{\testcase}\label{subsubsec:trustworthy-concepts-testing-testenv-testcase}
Un \testcase\ per un controllo è descritto da una tupla come segue,
in corsivo gli elementi non obbligatori: \testfulltuple.
\begin{description}
    \item[\testin] Definisce gli input, in formato JSON, da passare al controllo per l'esecuzione.
    L'input include, tipicamente, almeno il target, sotto forma di indirizzo IP o URL. Ulteriori
    parametri variano per ciascun controllo. 
    \item[\testout] Definisce gli output che ci si aspetta che il controllo ritorni per
    l'\testin\ specificato. L'output comprende sia gli \extradata, sia il risultato Booleano indicante
    se il controllo ha avuto successo o meno. Questo parametro è opzionale in quanto non si vuole
    forzare a scrivere esattamente un output, soprattutto nelle prime fasi dello sviluppo del controllo,
    in cui è prematuro definire un possibile output.
    \item[\testconf] Definisce le configurazioni per il test. Si tratta di un file JSON, che deve
    essere chiamato \testjson. Le configurazioni possibili sono le seguenti.
    \begin{itemize}
        \item Nome del file contenente l'input del test, ovvero il parametro \testin.
        Il nome di default è \testinputjson;
        se si usa tale nome allora non occorre specificarlo nella configurazione.
        \item Nome del file contenente l'output atteso del test, ovvero il parametro \testout.
        Il nome di default è
        \testoutputjson; se si usa tale nome allora non occorre specificarlo nella configurazione.
        Nel caso in cui tale file non esista, si considera l'output come assente, quindi da non verificare.
        \item Nome del file contenente la descrizione del target da creare e poi distruggere alla file del
        test, ovvero il parametro \testcompose. Il nome di default è \dockercomposefile;
        se si usa tale nome allora non occorre specificarlo nella configurazione. Nel caso
        in cui tale file non esista, la fase di creazione e distruzione dell'ambiente viene saltata.
        Ciò è utile nei casi in cui il target del testing non è un ambiente creato \emph{on the fly}
        ma un ambiente di produzione effettivo.
        \item Tempo di attesa che intercorre tra quando si crea il target di test e quando si fa lancia
        l'esecuzione del controllo verso di esso. Ciò è utile nei casi in cui il target del testing non
        sia immediatamente pronto ma occorra qualche secondo affinché lo sia. Ad esempio, in ambienti di test
        moderatamente complessi in cui occorre far partire più servizi, alcuni dei quali computazionalmente
        onerosi, è necessario dare loro tempo di partire.
        Il valore di default è $0$; se si usa tale valore allora non occorre specificarlo.
    \end{itemize}
    \item[\testcompose] Il file da utilizzare per la creazione del target. Tale file deve descrivere,
    in accordo alla tecnologia con cui poi viene effettivamente realizzato, tutti gli step ed i servizi
    necessari alla creazione del target di test. 
\end{description}

Le condizioni di successo e fallimento di \testcase\ $t$ per un controllo $c$ sono definite come segue.
Con $c.Out$ si intende l'output ottenuto dal controllo, con $E$ l'insieme degli errori derivanti dall'esecuzione
di $c$.

\begin{enumerate}
    \item \testsucess: $((t.$\testout\ $!=$ \nil$) \Rightarrow (t.$\testout$\ =\ c.$\testout$))$ $\land$
    $\vert E\vert$ $=$ $0$
    \item \testfailure: altrimenti. 
\end{enumerate}

Il primo punto esprime il concetto di \testsucess\ nel seguente modo. Se il \testcase\ ha un output
(parte sinistra dell'implicazione), allora l'output atteso deve essere uguale all'output ottenuto
(parte destra dell'implicazione). L'implicazione è vera anche se il \testcase\ non ha
nessun output atteso. Tutto ciò viene combinato in $\land$ con un'ulteriore condizione, cioè
che non vi siano stati errori di esecuzione. Ciò viene espresso con il richiedere che la cardinalità
dell'insieme $E$ degli errori sia uguale a $0$.

\subsubsection{\testenv}\label{subsubsec:trustworthy-concepts-testing-testenv-testenv}
Un \testenv\ $TE$ per un controllo è un insieme di \testcase. Il successo della fase di testing di un controllo
è definito dal successo dell'esecuzione del relativo \testenv, come segue.

\begin{itemize}
    \item \testsucess: $\forall$ $t$ $\in$  $TE$ $success(t)$
    \item \testfailure: $\exists$ $t$ $\in$ $TE$ $! success(t)$
\end{itemize}

\subsubsection{\testexec}\label{subsubsec:trustworthy-concepts-testing-testenv-testenv-testexec}
Il componente \testexec\ è l'effettivo esecutore della fase di testing. Esso si occupa di quattro
attività principali per ciascun \testcase\ del \testenv\ del controllo:
\num{1} creazione del target di test come definito dal parametro \testcompose\ se presente,
\num{2} esecuzione del controllo passando come input ciò che è definito dal parametro \testin, attesa della sua
esecuzione e recupero dell'output,
\num{3} distruzione del target di test creato al punto \num{1}, se presente,
\num{4} confronto dell'output ottenuto al punto \num{2} con quanto definito dal parametro \testout\ se presente.

Nel dettaglio, le fasi si svolgono come segue.

\begin{description}
    \item[1. Creazione target di test] Il \testexec\ esegue i comandi necessari
    alla creazione del target per il \testcase\ corrente. Sfruttando il fatto che
    ciascun ambiente possa essere creato allo stesso modo indipendentemente dallo sua complessità,
    il \testexec\ non deve avere comprensione del target. Esso deve solo
    eseguire i comandi necessari alla creazione del target, che sono sempre gli stessi.
    Per sapere come si chiama il file contenente le informazioni sull'ambiente da creare
    legge il file di configurazione. Prima di procedere con le attività successive,
    aspetta per l'eventuale tempo necessario, in accordo a quanto definito nella configurazione
    \testconf. Nel caso in cui non vi sia alcun target di test, questa fase viene semplicemente
    saltata.
    \item[2. Esecuzione test] Poiché i controlli sono dei container Docker, per la fase di esecuzione
    del controllo occorre utilizzare Docker stesso. Mediante i comandi appositi, il \testexec\ 
    fa partire il container del controllo, e vi passa l'input via standard input. Aspetta che
    l'esecuzione finisca, e recupera l'output.  
    \item[3. Distruzione target di test] Se il target di test è stato creato, il \testexec\
    esegue ora i comandi necessari per distruggerlo, liberando quindi risorse. 
    \item[4. Confronto] L'ultima cosa da fare è confrontare l'output dell'esecuzione del controllo
    con l'output definito dal parametro \testout. Durante questa fase di confronto, il \testexec\ 
    deve essere in grado di fare un confronto intelligente tra due oggetti JSON. Ad esempio
    è lecito che i due oggetti da confrontare abbiano una formattazione diversa, ciò
    che conta è la struttura degli oggetti, le varie chiavi ed i valori.
    Se il confronto va a buon fine, il \testcase\ ha successo.
    Nel caso in cui il file contenente l'output, cioè \testoutputjson, non esista, questa fase
    di confronto viene saltata, e, a meno che non vi siano stati errori nell'esecuzione,
    si considera il \testcase\ come concluso con successo.
\end{description}
