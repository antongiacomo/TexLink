\section{Processo di evaluation}\label{sec:mooncloud-evaluation}

\subsection{Entità}\label{subsec:mooncloud-evaluation-entities}

Il processo di valutazione (\emph{evaluation}) gestito da Moon Cloud coinvolge diverse entità, come segue.

\begin{description}
    \item[Controllo] Un controllo si occupa di rilevare, mediante raccolta di evidenze presso il target,
    il rispetto o meno di una proprietà non funzionale. Nel caso in cui tale proprietà sia rispettata,
    il controllo ritornerà \true, indicando il successo della verifica; viceversa, \false\
    modella il fatto che tale proprietà non è stata rilevata. Le evidenze raccolte sono dette
    \extradata.  
    \item[\aer] Una \aer\ rappresenta una versione estesa del \certmodelt, comprendente più proprietà (Definizione~\ref{def:methodology-certmodelt}).
    Essa descrive il modello di una \emph{evaluation}, mediante una formula Booleana. Visto che ciascun
    controllo modella una singola proprietà, la formula di una \aer\ modella l'insieme di proprietà di tale certificato.
    Il certificato è valido, e quindi viene rilasciato, solo se la sua formula viene valutata \true.
    Si noti che la formula potrebbe contenere degli \evor, pertanto non è richiesto che tutte
    le proprietà contenute nella formula siano rispettate per poter rilasciare il certificato.
    Le formule delle \aer\ sono predisposte dal team di Moon Cloud e salvate nel \moonccomp{Model DB}.
    \item[\test] L'istanziazione di un controllo su uno specifico target corrisponde ad un \test.
    Si tratta di un nuovo oggetto, salvato nel \moonccomp{Model DB}, che contiene gli input per il controllo
    che istanzia.
    \item[\er] Una \er\ rappresenta una versione estesa del \certmodeli, comprendente più proprietà (Definizione~\ref{def:methodology-certmodeli}).
    Essa consiste nell'istanziazione di una \aer\ su uno specifico
    target, e viene anche detta \emph{User Evaluation Rule}.
    Si tratta di un nuovo oggetto, salvato nel \moonccomp{Model DB}, che contiene la formula della \aer, con il
    riferimento non più ai relativi controlli ma alle loro istanze, cioè ai \test. È anche possibile
    istanziare più \aer\ assieme, creando una \er\ composta più complessa, in cui la formula Booleana
    modella non solo l'insieme di proprietà di ciascuna \aer, ma anche come le \aer\ si compongono
    nel creare la \er\ in questione.
\end{description}

\subsection{Processo}\label{subsec:mooncloud-evaluation-process}

Il processo di \emph{evaluation}, per un target nella cloud, funziona nel seguente modo. Tutte
le interazioni tra Moon Cloud e l'utente passano tramite la \moonccomp{Dashboard}. 

Come prima cosa, l'utente registra i target che saranno oggetto dell'analisi all'interno del framework.
Nella fase di registrazione, l'utente specifica a quali \mooncloudproject\ e \mooncloudzone,
concettualmente due contenitori, il target appartiene.
Di seguito, sceglie, tra la lista di \aer\ possibili, quale intende eseguire verso quale target.
% Moon Cloud offre anche la possibilità di comporre tra loro \aer\ per creare formule Booleane più complesse;
% per semplicità si considera un processo di evaluation composto da una singola \aer, a sua volta
% composta da un unico controllo.
Dopo aver selezionato l'\aer\ di interesse, l'utente specifica gli input necessari. In particolare,
l'utente definisce quale è il target dell'analisi, scegliendolo tra quelli precedentemente inseriti,
ed ulteriori parametri necessari, variabili a seconda dei controlli coinvolti.

Dopo aver confermato la richiesta d'esecuzione, la \moonccomp{Dashboard}\ fa una richiesta alle \moonccomp{Main API},
richiedendo l'esecuzione della \aer\ scelta. I successivi passi, svolti all'interno del framework,
sono i seguenti.

\begin{description}
    \item[1. Creazione oggetti] Le \moonccomp{Main API}\ istanziano i controlli contenuti nell'\aer\, creando per ciascuno
    un nuovo oggetto \test.
    Si istanzia quindi l'\aer\ stessa, creando un nuovo oggetto \er\ referenziando il \test\ appena creato.
    I nuovi oggetti sono quindi salvati all'interno del \moonccomp{Model DB}.
    \item[2. Selezione \moonccomp{Dispatcher}] Le \moonccomp{Main API}\ selezionano il \moonccomp{Dispatcher}\ da utilizzare
    per l'esecuzione corrente, sulla base della locazione del target e della sua raggiungibilità.
    Se esso si trova in una rete privata non raggiungibile dall'esterno, allora in tale rete sarà stato
    \emph{deployato} un \moonccomp{Execution Manager} dedicato; si sceglierà quindi un \moonccomp{Dispatcher}\ in grado
    di scrivere sulla coda di tale \moonccomp{Execution Manager}.
    Altrimenti, se il target è direttamente raggiungibile, vuol dire che si utilizzerà un \moonccomp{Execution Manager}\
    di default, e quindi un qualsiasi \moonccomp{Dispatcher}\ che sia in grado di scrivere sulla coda usata da quest'ultimo.
    \item[3. Invio richiesta da \moonccomp{Main API}\ a \moonccomp{Dispatcher}] Una volta selezionato il \moonccomp{Dispatcher},
    le \moonccomp{Main API}\ inviano ad esso una richiesta di esecuzione, contenente tutti i dati necessari all'esecuzione. 
    \item[4. Invio richiesta da \moonccomp{Dispatcher}\ a \moonccomp{Execution Manager}] Il \moonccomp{Dispatcher}\ riceve la richiesta proveniente
    dalle \moonccomp{Main API}, la modifica, aggiungendovi eventualmente ulteriori parametri necessari, e la scrive
    sulla coda su cui è configurato per scrivere.
    \item[5. Richiesta esecuzione \moonccomp{Execution Manager}] L'\moonccomp{Execution Manager}\ riceve, dalla coda su cui legge, la richiesta
    di esecuzione. Tra le altre cose, la richiesta contiene il nome dell'immagine Docker da eseguire~\footnote{In Docker,
    ciascuna immagine è identificata da un \emph{nome} e da un \emph{tag}, che ne specifica la versione.
    Di seguito un esempio. Si supponga che l'URL del registry usato sia \emph{repository.v2.moon-cloud.eu:4567},
    che il nome dell'immagine sia \emph{tls-check}, e la versione sia \emph{latest}, il nome completa
    di tale immagine è: \emph{repository.v2.moon-cloud.eu:4567/tls-check:latest}.}, e l'input da passare ad
    essa al momento della sua esecuzione. L'\moonccomp{Execution Manager}\ quindi contatta il demone Docker in esecuzione
    sul \moonccomp{Worker}\ designato mediante le API di Docker, richiedendo l'esecuzione di tale immagine, con gli
    input richiesti. Nel dettaglio, gli input, in formato JSON, sono passati via standard input alla probe.
    \item[6. Esecuzione] Il demone Docker in esecuzione sullo \moonccomp{Worker}\ fa partire il container richiesto.
    Il container fà le proprie attività, e produce i risultati. Nel dettaglio, le evidenze raccolte
    dalla probe (\extradata), vengono scritte direttamente da essa in formato JSON nell'\moonccomp{Evidence DB}.
    Nell'\moonccomp{Evidence DB}\ la probe stessa scrive anche il risultato Booleano della sua esecuzione,
    \true\ se la proprietà che doveva verificare è stata verificata, \false\ altrimenti.
    \item[7. Valutazione complessiva] In maniera asincrona rispetto al resto dell'esecuzione, l'\moonccomp{Evidence Analyzer}\ si occupa della valutazione complessiva delle \er. Esso, allo scadere di un determinato
    timeout, esegue una serie di query verso l'\moonccomp{Evidence Analyzer}\ ed il \moonccomp{Model DB}, per vedere se ci sono delle nuove
    \er\ la cui esecuzione è terminata e per cui manca la valutazione della formula. L'\moonccomp{Evidence Analyzer}\
    valuta quindi la formula di ciascuna \er, aggiornandone lo stato nel \moonccomp{Model DB}.
    \item[8. Visualizzazione risultati] Nel frattempo, l'utente fa le proprie azioni sulla \moonccomp{Dashboard}. Tra le
    varie API, offerte dalle \moonccomp{Main API}\ e chiamate dalla \moonccomp{Dashboard}, ve ne sono anche alcune che mostrano
    il risultato dell'esecuzione delle \er\ lanciate dall'utente. Quando lo stato delle \er\ cambia
    in \emph{Completed}, l'utente può vedere se tale \er\ ha avuto successo, e se quindi il certificato
    relativo ad essa è stato rilasciato, e quali sono le evidenze raccolte dai vari controlli.
\end{description}

\subsection{Un esempio}\label{subsec:mooncloud-evaluation-example}

% Si consideri una evaluation il cui obiettivo è valutare la compliance della configurazione
% di un server, in merito a $i)$ configurazione SSH, usando come metro di confronto le linee guida
% emanate da Mozilla~\cite{mozilla-openssh}, $ii)$ applicazione di vari best-practice di un sito
% web, usando come metro di confronto ancora delle linee guide emanate da Mozilla~\cite{mozilla-observatory}.

% \noindent \textbf{Entità} Si utilizzano due \aer\, chiamate rispettivamente
% \emph{SSH-Compliance} e \emph{Observatory}. Ciascuna di esse è a sua volta composta da un singolo
% controllo, responsabile della valutazione del rispetto delle proprietà di compliance rispetto
% alle due linee guida descritta.
% Ai fini dell'esempio, si considera che le due \aer\ sono destinate ad uno stesso target. In tal caso,
% può essere conveniente comporle in una singola \er, usando come connettore logico una \evand.
Si consideri una \emph{evaluation} il cui obiettivo è valutare la configurazione TLS (\emph{Transport Layer Security})
di un server. Si utilizza una \aer\ predisposta per tale scopo, composta da due controlli connessi
usando una \evand. I due controlli sono: $i)$ un controllo per valutare il setup generale di TLS, verificando,
ad esempio, che si usino solo le versioni più recenti del protocollo e $ii)$ un controllo specifico
che cerchi dei bug nell'implementazione del protocollo, come ad esempio il bug \heartbleed\ di
\emph{OpenSSL}~\cite{heartbleed}. I due controlli danno esito positivo quando la configurazione è
buona e non vi sono vulnerabilità a tale bug, rispettivamente. Nel complesso, l'\aer\
avrà successo solo se entrambi i due controlli hanno successo, poiché sono legati mediante \evand.
I passi da seguire sono i seguenti. L'\aer\ è chiamata \tlsstrength, i due controlli rispettivamente
\tlsconfig\ e \tlsbug, i nomi delle loro immagini Docker sono \emph{repository.v2.moon-cloud.eu:4567/probes/sslyze} e
\emph{repository.v2.moon-cloud.eu:4567/probes/heartbleed}.
Si assume infine che il target sia raggiungibile su rete pubblica.

L'utente registra il target oggetto di valutazione, e seleziona la \aer\ da utilizzare. La \moonccomp{Dashboard}\
presenta una form in cui l'utente inserisce tutte le configurazioni necessarie per il funzionamento
della verifica, ed una volta confermato, nel framework accadono le seguenti cose.

\begin{description}
    \item[1) Creazione oggetti] Le \moonccomp{Main API}\ istanziano i due controlli che compongono l'\aer\ \tlsstrength,
    creando due \test\, ciascuno comprende il nome dell'immagine Docker da eseguire,
    unitamente agli input da passare al container. Si crea quindi una nuova \er\, che mantiene il riferimento
    ai due \test. Tutti e tre gli oggetti sono salvati nel \moonccomp{Model DB}.
    \item[2) Selezione \moonccomp{Dispatcher}] Le \moonccomp{Main API}\ selezionano il \moonccomp{Dispatcher}\ da utilizzare nell'esecuzione. Trattandosi
    di un target pubblicamente raggiungibile, si utilizza un \moonccomp{Execution Manager}\ standard: le \moonccomp{Main API}\ scelgono
    quindi uno dei \moonccomp{Dispatcher}\ disponibili responsabili di un \moonccomp{Execution Manager}\ di questo tipo. 
    \item[3) Invio richiesta da \moonccomp{Main API}\ a \moonccomp{Dispatcher}] Le \moonccomp{Main API}\ preparano la richiesta di esecuzione da inviare al \moonccomp{Dispatcher},
    mediante le API REST offerte dal \moonccomp{Dispatcher} stesso. Tale richiesta, in formato JSON, contiene
    informazioni quali: input per i singoli container rappresentanti i controlli, nomi delle immagini Docker,
    eventuali parametri per l'esecuzione periodica (in questo caso l'esecuzione non è periodica ma \oneshot).
    La richiesta viene infine inviata.
    \item[4) Invio richiesta da \moonccomp{Dispatcher}\ a \moonccomp{Execution Manager}] Il \moonccomp{Dispatcher}\ riceve la richiesta proveniente
    dalle \moonccomp{Main API}. Poiché l'evaluation richiesta non deve essere schedulata, il \moonccomp{Dispatcher}\ non deve apportarvi
    modifiche particolari. Scrive quindi la richiesta di esecuzione sulla coda.  
    \item[5) Richiesta esecuzione \moonccomp{Execution Manager}] L'\moonccomp{Execution Manager}\ designato per l'esecuzione preleva dalla
    coda il nuovo \emph{task}. L'\moonccomp{Execution Manager}, quindi, invia una richiesta al demone Docker in esecuzione sullo \moonccomp{Worker}. Visto
    che l'\er\ contiene due controlli, quindi due container, l'\moonccomp{Execution Manager}\ effettua due richieste mediante le API
    di Docker. In ciascuna si specifica quale immagine deve essere eseguita (\emph{repository.v2.moon-cloud.eu:4567/probes/sslyze} o
    \emph{repository.v2.moon-cloud.eu:4567/probes/heartbleed}), e l'input da passare a ciascuna.
    \item[6) Esecuzione] Docker esegue le due immagini, istanziando due container, sullo \moonccomp{Worker}. Le due probe hanno
    entrambe successo, ovvero rilevano che TLS è configurato correttamente (probe \tlsconfig) e non vi è vulnerabilità
    a \heartbleed; essi ritornano quindi \true. Tali \extradata\ sono scritti dai due container direttamente nell'\moonccomp{Evidence DB}.
    \item[7) Valutazione complessiva] Parallelamente alle attività svolte dai componenti citati, l'\moonccomp{Evidence Analyzer}\
    effettua delle query periodiche sull'\moonccomp{Evidence DB} per verificare se vi sono nuove \er\ i cui relativi \test\
    siano conclusi, e per cui occorre valutare la formula Booleana di tale \er. In questo caso, l'\moonccomp{Evidence Analyzer}\
    rileva la presenza della \er\ in questione. Esso recupera i risultati dell'esecuzione dei due \test, e valuta la formula.
    Poiché la formula è un \evand\ del risultato dei due \test, ed entrambi i risultati sono \true, il risultato
    finale dell'\er\ è \true. Questo valore viene infine scritto nel \moonccomp{Model DB}.   
    \item[8) Visualizzazione risultati] Fino a che il risultato finale dell'\er\ non è stato computato, l'utente non
    può accederne ai risultati, la vede in \emph{Pending}. Quando finalmente il risultato finale è stato calcolato
    dall'\moonccomp{Evidence Analyzer}, sulla \moonccomp{Dashboard} viene mostrata l'\er\ nella sua interezza, con gli input richiesti dall'utente,
    gli \extradata, ed il risultato finale, in questo caso \emph{Success}.
\end{description}
