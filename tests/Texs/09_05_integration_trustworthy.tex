\section{Integrazione del nuovo controllo}\label{sec:case-study-real-integration}
Supponendo che il team DevOps abbia risolto il problema relativo alle dipendenze mostrato
nella Sezione precedente, l'\aer\ \aerservicetime\ può essere infine integrata in Moon Cloud.
Poiché occorre che i controlli di Moon Cloud siano \emph{trustworthy} (Sezione~\ref{subsec:methodology-framework-requirements}),
è necessario definire del testing, in particolare occorre definire un \testenv\ (Sezione~\ref{sec:trustworthy-concepts}).

\subsection{Definizione del \testenv}
Un \testenv\ è composto da un insieme di \testcase, ciascun \testcase\ è una tupla \testfulltuple, i
cui componenti sono definiti come segue~\footnote{Per ulteriori dettagli si rimanda alla Sezione~\ref{sec:trustworthy-concepts}.}.

\begin{itemize}
    \item \testin: l'input del controllo nella fase di testing.
    \item \testout: l'output che ci si aspetta il controllo ritorni, avendo come input \testin.
    \item \testconf: le configurazioni per guidare l'esecuzione del \testcase\ in questione.
    \item \testcompose: il file per \dockercompose\ da utilizzare per l'eventuale creazione
    di target di test.
\end{itemize}

Per il testing di \probeservicetime\ si utilizza un \testenv\ composto da due \testcase.
Entrambi i \testcase\ non usano il parametro \testout. Ciò è dovuto al fatto che il software \ciexecutor,
nel confrontare l'output ottenuto con quello atteso, effettua un confronto senza differenziare
tra \extradata\ e risultato Booleano. In questo caso, gli \extradata\ contengono il tempo di risposta
effettivo del target, ed è un valore che non è possibile conoscere a priori in maniera precisa, nemmeno
usando un target creato mediante il parametro \testcompose.
Vista la relativa inutilità di usare target basati su \dockercompose, anche tale parametro non viene
utilizzato; si usano invece target pubblici.
La definizione dei \testcase\ viene mostrata in tabella~\ref{tbl:probe-service-time-check-testenv}.

\begin{table*}[h!]
    \centering
    \caption[\testenv\ per la probe \probeservicetime]{\testenv\ per la probe \probeservicetime.
    I parametri \testcompose\ e \testout\ non vengono utilizzati, così come \testconf, poiché si usano solo
    configurazioni di default. }
    \label{tbl:probe-service-time-check-testenv}
    \begin{tabular}{|l|c|}
        \hline
        \testcase & \testin\\
        \hline
        \testcase\ $1$ & Target: \url{https://google.it}, tempo massimo: $300$ ms\\
        \hline
        \testcase\ $2$ & Target: \url{https://google.it}, tempo massimo: $50$ ms\\
        \hline
    \end{tabular}
\end{table*}

Un \testenv\ corrisponde ad una singola \emph{repository}, memorizzata all'interno della \emph{repository} Gitlab,
usata per lo sviluppo di Moon Cloud. Per testare la probe \probeservicetime, quindi, è stata
approntata una \emph{repository} apposita, contenente il \testenv\ in questione.
Essa è composta da due cartelle, come mostrato in Figura~\ref{fig:probe-service-time-dirtree}.

\begin{figure}[t]
    %\centering
    \dirtree{%
.1 test\_1.
.2 input.json.
.1 test\_2.
.2 input.json.
}
    \caption[Struttura di directory del \testenv\ per il controllo \probeservicetime]{Struttura di directory del \testenv\ per il controllo \probeservicetime.}
    \label{fig:probe-service-time-dirtree}
\end{figure}


Il contenuto del file \filename{test\_1/input.json} è mostrato nel Codice~\ref{lst:test1-inputjson}. A sua volta, il contenuto del file
\filename{test\_2/input.json} è simile, ad eccezione del parametro \key{max\_time}.

\begin{listing}[h!]
    \inputminted{json}{code/09_05_service_response_time_testcase.json}
    \caption[Contenuto del file \emph{test\_1/input.json}]{Contenuto del file \emph{test\_1/input.json}.}
    \label{lst:test1-inputjson}
\end{listing}


Per poter eseguire la fase di testing con il \testenv\ appena descritto, occorre inserire un apposito
\gitlabjob\ nella pipeline del controllo, come mostrato in Sezione~\ref{sec:trustworthy-new-pipeline}.
In particolare, si definisce un \gitlabjob\ chiamato \gitlabjobname{control\_testing}, facente parte
dello \gitlabstage\ di \giltabstagename{test}. Si noti che si mantiene comunque il \gitlabjob\ di
unit testing. Il file \gitlabci\ si presenta come segue.
\inputminted[highlightlines={5-10, 39-53}]{yaml}{code/09_05_service_response_time_trustworthy_gitlabci.yml}

In Figura~\ref{fig:probe-service-time-check-job} si riporta l'output di Gitlab durante l'esecuzione del \gitlabjob\ \gitlabjobname{control\_testing}.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.3]{img/09_05_trustworthy_execution_ci_example.png}
    \caption[Esecuzione del \gitlabjob\ \gitlabjobname{control\_testing} per la probe \probeservicetime.]{Esecuzione del
    \gitlabjob\ \gitlabjobname{control\_testing} per la probe \probeservicetime. Per esigenze
    dimostrative, il percorso della \emph{repository} contenente i \testcase\ è diverso da quello mostrato nel file \gitlabci}
    \label{fig:probe-service-time-check-job}
\end{figure}

\noindent \textbf{Discussione} La fase di passaggio da un controllo \emph{esterno}
ad uno interno ed ufficiale di Moon Cloud non è automatica, in ogni caso tale passaggio
avviene solo se le attività di assurance svolte da Moon Cloud nella pipeline
del controllo \emph{esterno} vanno a buon fine.
Una volta che avviene il passaggio, è necessario che il nuovo controllo sia
\emph{trustworthy}, e per farlo è necessario avere una rigorosa fase di testing del
controllo in un ambiente controllato, come discusso nel Capitolo~\ref{trustworthy}.
Infine, una volta che il controllo è stato integrato, la fase di certificazione discussa
in Sezione~\ref{sec:case-study-integration} non viene più eseguita.
Ciò è stato fatto per semplicità, tuttavia è possibile che tale fase continui
ad essere eseguita anche ad integrazione del controllo avvenuta.
