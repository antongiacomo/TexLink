\section{Libreria di alto livello}\label{sec:api-driven-library}
Per quanto il documento OpenAPI  descrivente le API REST di Moon Cloud
possa aiutare un team DevOps nell'integrare il framework all'interno delle proprie pipeline CI/CD,
esso è comunque limitato.
In particolare, il team si ritroverebbe comunque con il dover inserire una serie di invocazioni HTTP, che, per quanto
ben documentate, sono pur sempre invocazioni HTTP. Ciò significa che il team deve innanzitutto scegliere
un client HTTP a linea di comando da utilizzare, tipicamente \emph{curl}, ma soprattutto deve creare il payload
delle richieste e fare il parsing di ciò che viene ritornato. Eseguire queste attività a linea di comando
può non essere semplice.

Pertanto il framework deve fare un passo ulteriore per facilitare l'integrazione, deve fornire un SDK che
possa essere utilizzato dal team DevOps.
Nel fornire questo SDK, Moon Cloud può sfruttare il fatto di avere il documento OpenAPI, ed affidarsi a
generatori di codice lato client.

\noindent \textbf{Il generatore usato} Si è utilizzato lo strumento
\openapigen~\cite{openapi-generator}. Esso è scritto in Java, e nasce come \emph{fork}
di un altro strumento, \emph{Swagger Codegen}~\cite{swagger-codegen}. Supporta un'ampia quantità di
linguaggi e framework, tra cui i seguenti lato server: C\#, Go, Javascript, PHP,
Python, Ruby, Rust. Per il linguaggio Java in particolare si supportano numerosi
framework, tra cui \emph{MSF4J} (\url{https://wso2.com/products/microservices-framework-for-java/})
e \emph{Spring} (\url{https://spring.io/}).
Ampio è anche il supporto per linguaggi di programmazione lato client, tra essi si segnala anche Typescript,
che non è direttamente supportato lato server.

\subsection{Codice creato}\label{sec:api-driven-library-code}
Il codice generato da \openapigen, indipendentemente dal linguaggio, segue sempre lo stesso paradigma. Si generano,
in particolare, tre tipi di \emph{componenti}. Concretamente, possono essere \emph{classi}, \emph{struct} o
\emph{interfacce}, a seconda del linguaggio di programmazione usato. Le tre tipologie sono: \componentname{Model}
che modellano le \emph{risorse}, \componentname{AbstractOperation}\ che fornisce metodi di basso livello
per effettuare \emph{operazioni}, e \componentname{Operation}\ che realizza concretamente le \emph{operazioni}
sulle \emph{risorse}. Di seguito sono descritti nel dettaglio.

\begin{description}
    \item[\componentname{Model}] Per ogni \restresource\ REST si genera un \componentname{Model}. Ciascun \componentname{Model}
    è sostanzialmente una classe di \emph{soli dati}, i cui attributi sono i vari campi di cui la \restresource\ si compone.
    Di seguito si mostra un esempio di un \entityname{Target}\ realizzato mediante una \emph{struct} in Go
    ed in una \emph{interface} di Typescript. Al di là delle ovvie differenze dettate dall'uso di due linguaggi di
    programmazione diversi, le similitudini sono evidenti.
    \inputminted{go}{code/06_03_target.go}
    \inputminted{ts}{code/06_03_target.ts}
    \item[\componentname{AbstractOperation}] Un componente astratto contenente una serie
    di metodi necessari per effettuare richieste HTTP codificandone correttamente i parametri, decodificare
    le risposte nei \componentname{Model}, gestire l'autenticazione, mantenere in memoria una configurazione
    da usare durante le varie richieste. Un esempio in Go è il seguente, poiché è abbastanza complesso si mostrano
    solo alcuni estratti rilevanti. Il generatore di codice ha generato una \emph{struct} chiamata \proglangkey{APIClient},
    che mette a disposizione metodi \emph{di basso livello} per altri componenti. Uno di questi metodi si chiama
    \proglangkey{callAPI}, che viene mostrato in seguito.
    \inputminted[firstline=191, lastline=215]{go}{code/06_03_abstract_operation.go}
    \item[\componentname{Operation}] Per ciascun \emph{gruppo} di \restoperations\ su \restresource, \openapigen\ genera un componente
    che, usando le funzionalità definite nell'\componentname{AbstractOperation}, effettua le operazioni su tali
    risorse. Il raggruppamento è stabilito sulla base degli eventuali \openapitag\ definiti nel documento.
    Un esempio in Go è il seguente, in cui si mostra il metodo \proglangkey{TargetDelete}\ definito sulla \emph{struct}
    \proglangkey{TargetApiService}. Tale \emph{struct} utilizza il metodo \proglangkey{callAPI} (\codeline{162}),
    che viene fornito dalla \emph{struct} \proglangkey{APIClient}, ovvero il componente \componentname{AbstractOperation}.
    \inputminted[firstline=117, lastline=182, highlightlines=162]{go}{code/06_03_target_operation.go}
\end{description}

Il codice client generato da \openapigen\ è una libreria completa e pronto all'uso. Ciò che manca è la parte esecutiva,
ovvero scrivere il programma che effettivamente utilizzi tale libreria.

Utilizzare un generatore di codice per generare un SDK lato client è utile e comodo.
Esso consente di
evitare di scrivere molto codice, risparmiando tempo.
Inoltre, grazie al fatto che molti linguaggi di programmazione
diversi sono supportati, è possibile generare codice per il framework e linguaggio che si preferisce, senza
vincolare ad una scelta particolare.
Il codice generato è, inoltre, \emph{idiomatico} per il linguaggio di programmazione usato, cioè utilizza
al meglio i costrutti che ciascun linguaggio fornisce.
Infine, generatori come \openapigen\ sono estendibili, ovvero è possibile aggiungere il supporto per
ulteriori linguaggi, ed anche modificare come avviene la generazione per uno specifico linguaggio.

In Tabella~\ref{tbl:openapigen-generated-code} si mostra, per diversi linguaggi di programmazione, quante righe di codice effettivo
sono state generate con \openapigen~\footnote{Per il conteggio delle righe di codice ci si è affidati allo strumento
\emph{cloc} (\url{https://github.com/AlDanial/cloc}).}. Tutte quelle righe di codice sono state generate in poche secondi,
in maniera idiomatica, e sono pronte per essere integrate in un qualsiasi software.
Vi sono differenze vistose nel numero di righe codice generato, si va dalle $1657$ di Typescript, alle $9147$ di Java.
La ragione sta nel fatto che il codice generato è idiomatico. Infatti, il codice Java contiene molte annotazioni,
metodi \emph{get} e \emph{set}, che fanno crescere molto la dimensione finale del codice. D'altro canto, il
codice Typescript e Rust è più compatto, poiché non vi sono annotazioni, e gli attributi dei \componentname{Model}
sono direttamente visibili, senza usare \emph{get} e \emph{set}.

\begin{table}
    \centering
    \caption{Quantità di codice lato client generato automaticamente con \openapigen.}
    \label{tbl:openapigen-generated-code}
    \resizebox{.85\textwidth}{!}{
    \begin{tabular}{|c|c|c|}
        \hline
        Linguaggio di programmazione & Nome del target in \openapigen & Righe di codice\\
        \hline
        \hline
        Go & \openapigentarget{go} & $5502$\\
        Java & \openapigentarget{java} & $9147$\\
        Kotlin & \openapigentarget{kotlin} & $3205$\\
        Python & \openapigentarget{python} & $7981$\\
        Rust & \openapigentarget{rust} & $1888$\\
        Typescript & \openapigentarget{typescript-aurelia} & $1657$\\
        \hline
    \end{tabular}
    }
\end{table}

\subsubsection{Esempio}\label{subsubsec:api-driven-library-example}
Di seguito si mostra un esempio di codice per le API REST di Moon Cloud, scritto in Go, che
consente di ottenere un certo target. È stato scelto Go poiché verrà usato anche nel Capitolo~\ref{cicd-native}.
Per poter interagire con le API è necessario importare il package \proglangkey{openapi} (\codeline{5}),
ovvero il package contenente i file generati da \openapigen.
È necessaria una fase di setup in cui si configura un nuovo oggetto di tipo \proglangkey{openapi.APIClient},
corrispondente al componente \componentname{AbstractOperation}; tale fase di setup viene fatta
nella funzione \proglangkey{createClient}\ (\codelines{11}{30}).
Una volta configurato il client, è sufficiente chiamarne i metodi esposti.
Nell'esempio mostrato, in particolare, occorre solo chiamare il metodo \proglangkey{TargetRead},
che è stato generato automaticamente, per ottenere un certo target (\codeline{36}): tutta la
complessità dell'interazione con le API REST è nascosta al programmatore.
\inputminted[highlightlines={4-5, 28, 36}]{go}{code/06_03_target_ops.go}
