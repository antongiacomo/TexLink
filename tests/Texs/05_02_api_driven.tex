\section{Un framework API-driven}\label{sec:api-driven-framework}
L'utilizzo di REST ed OpenAPI sono necessari per soddisfare il requisito di \emph{API-driven}, definito in
Sezione~\ref{subsubsec:methodology-framework-requirements-api-driven}. Tale requisito richiede che,
ai fini dell'integrazione di un framework
di assurance esterno in una pipeline, tale framework debbe esporre delle API aderenti ad un
certo standard. Nella fattispecie, Moon Cloud aderisce allo standard REST, quindi le API che
tale framework espone sono di tipo REST.

\subsection{Solo HTTP}\label{subsec:api-driven-framework-http}
Come prima cosa, si richiede che il framework esponga delle API che seguano
un paradigma standard, e che perciò consentano una facile integrazione all'interno della pipeline.
In particolare, per soddisfare questa richiesta, si utilizzano le API di Moon Cloud che espongono
una interfaccia REST.
Un primo livello di integrazione del framework all'interno di una pipeline CI/CD prevede, quindi, di inserire
nella pipeline una serie di chiamate HTTP, necessarie per effettuare un valutazione.
Ad esempio, in uno specifico \gitlabjob\ si inseriscono una serie di \gitlabscript\ che chiamano
le API REST di Moon Cloud. Un esempio semplificato è il seguente. Si esegue il comando
\emph{curl}\ per effettuare una chiamata HTTP alle API di Moon Cloud, assumendo che rispondano
alla URL \url{https://api.moon-cloud.eu}. Nel payload della richiesta si specificano i campi
necessari all'esecuzione della valutazione. In questo caso, la struttura dell'input non è veritiera,
è un semplice esempio per mostrare il concetto.

\begin{minted}{bash}
curl --header "Authorization: Token $TOKEN" \
     --header "Content-type: application/json" \
     -X POST \
     -d '{"aer": 3, "name": "monitoring", "tests": ["control": 7, "host": "http://internal.example.com"]}' \
     https://api.moon-cloud.eu/evaluation-rules/
\end{minted}

Tale comando va quindi inserito sotto la chiave \gitlabscripttt\ del file \gitlabci.

\subsection{Documentazione}\label{subsec:api-driven-framework-docs}
Nel momento in cui un team DevOps vuole integrare le funzionalità
di Moon Cloud nella propria pipeline, esso deve sapere effettivamente \emph{quali} API chiamare,
con quali parametri e formati. Per tale ragione dovrebbe esistere una documentazione a supporto.
Poiché le API di Moon Cloud sono REST, la documentazione deve seguire lo standard per la REST,
ovvero OpenAPI.
Moon Cloud è stato sviluppato non seguendo una metodologia model-driven, ovvero non si è iniziato
a sviluppare il componente \moonccompmainapi\ a partire dalla definizione dell'interfaccia da esporre.
Per poter far sì che Moon Cloud soddisfi i requisiti definiti, in particolare la presenza di
una documentazione standard, è stato necessario definire un documento OpenAPI.

Le API esposte da \moonccompmainapi\ sono numerose, poiché gestiscono tutte gli aspetti di Moon Cloud. Vengono
definite anche una serie di API ad uso interno, come API per registrare programmaticamente nuovi controlli
e nuove \aer, piuttosto che registrare nuovi \moonccompdisp. Visto l'elevato numero, non si
è ritenuto pratico scrivere il documento API a mano.

L'approccio infine scelto per la generazione del documento è basato sulla libreria \drfyasg~\cite{drf-yasg}.
Il componente \moonccompmainapi\ è scritto usando i framework \django\ e \djangorest. Quest'ultimo,
a partire dalle ultime versioni, ha integrato il supporto per la generazione automatica del documento
OpenAPI. Tuttavia, tale supporto è molto immaturo, in particolare è molto difficile
adattare la generazione del documento per una certa API. Ad esempio, è difficile aggiungere
un esempio di richiesta e di risposta valida, oppure specificare il valore di campi come \texttt{description}.
Per questo, dopo vari tentativi, ci si è spostati sulla libreria \frameworkname{drf-yasg}.
A differenza di \djangorest, \drfyasg\ supporta solo il formato OpenAPI versione 2,
il supporto alla versione 3 è in sviluppo.
Dall'altro lato invece, \drfyasg\ fornisce un'ottima documentazione, e consente di adattare
la generazione del documento OpenAPI per ciascuna singola API. Tra le altre cose, permette anche di
fare il \emph{mock} di porzioni del documento, consentendo agli sviluppatori
di scriverne delle parti direttamente a mano, qualora sia necessario.
Infine, il documento generato con \djangorest\ si è rivelato non essere nemmeno valido rispetto
alla specifica, mentre invece con \drfyasg\ la specifica è rispettata.

Il documento generato da \drfyasg\ è servito da uno specifico endpoint nelle \moonccompmainapi,
in particolare alla seguenti due URL.

\begin{itemize}
    \item \url{/openapi.json} serve il documento in formato JSON.
    \item \url{/openapi.yaml} serve il documento in formato YAML.
\end{itemize}

Di seguito si mostrano alcuni esempi del documento generato.

\noindent \textbf{Risorsa \entityname{FullTarget}} Un \entityname{FullTarget} modella il target di una valutazione.
E' composto da una serie di campi, tra cui \fieldname{project}\  e \fieldname{zone}\ (due contenitori),
l'\fieldname{hostname}, ed ulteriori campi. Ciascuno di essi viene poi descritto nel dettaglio,
specificando informazioni come ulteriori restrizioni sui valori possibili, se il campo è disponibile
in sola lettura, cioè se non può essere \emph{scritto}, ed altro.
\inputminted[firstline=1921, lastline=1979]{yaml}{code/06_02_openapi.yaml}

\noindent \textbf{Operazioni su \entityname{Target}} Di seguito si riportano due delle \restoperations\
possibili sulla risorsa \entityname{Target}. La chiave di più alto livello, ovvero \key{/target/},
identifica la URL. Essa ha due chiavi, \key{get}\ e \key{post}, che modellano le \restoperations\
che possono essere fatte sulla \restresource\ a quella URL. Si noti come, mediante l'uso delle direttive
\key{schema}\ e \key{\$ref}\ si specifica quale è lo schema della richiesta e della risposta, lo
schema viene definito in una parte separata del documento, favorendo la leggibilità ed
il riuso\footnote{Si noti che la sintassi per riferirsi ad uno schema è diversa rispetto a quanto
descritto in Sezione~\ref{subsec:api-driven-pre-req-openapi-spec}, questo perché il documento
OpenAPI di Moon Cloud è conforme alla versione \emph{v2}, quello descritto precedentemente, invece,
si riferisce alla versione \emph{OpenAPI v3}.}.
\inputminted[firstline=1040, lastline=1075]{yaml}{code/06_02_openapi.yaml}

\noindent \textbf{Documentazione grafica} Oltre ad una documentazione formale delle API di Moon Cloud,
occorre servirla anche in un formato orientato verso gli sviluppatori. In altre parole, occorre fornire una
visualizzazione grafica del documento OpenAPI.
A tale scopo è stato approntato un visualizzatore che utilizza \emph{ReDoc}~\cite{redoc}. Il visualizzatore
è \emph{deployato} come un container Docker, che visualizza il documento OpenAPI servito dalle \moonccompmainapi\ 
stesse.
Configurare il container Docker per \emph{ReDoc} è estremamente semplice, occorre solo specificare
nella variabile d'ambiente \envvar{SPEC\_URL} l'URL a cui il documento OpenAPI da visualizzare
viene servito. Nello specifico si usa il seguente comando:
\mintinline{bash}{docker run -d --rm -e SPEC_URL=https://api.moon-cloud.eu/openapi.json -p 8080:80 redocly/redoc}.\\
\emph{ReDoc} produce una visualizzazione grafica per il web; il risultato del rendering del documento
OpenAPI viene mostrato in figura~\ref{fig:redoc}.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{img/06_02_redoc.png}
    \caption[Visualizzazione delle API REST di Moon Cloud]{Visualizzazione grafica del documento OpenAPI delle API REST di Moon Cloud.}
    \label{fig:redoc}
\end{figure}

Per concludere, il documento OpenAPI delle API esposte da \moonccompmainapi\ è lungo ben $2047$ righe ed è piuttosto complicato,
questo evidenzia l'importanza di strumenti come \drfyasg, che consentono la generazione ispezionando il codice,
quasi senza l'intervento del programmatore.
