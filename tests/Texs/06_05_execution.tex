\section{Deployment ed uso nella pipeline}\label{sec:cicd-native-deployment}
\noindent \textbf{Deployment} La \emph{build} del \mooncloudcontainer, come nel caso del \ciexecutor, sfrutta
una \emph{multi-stage build} per ottimizzare la dimensione finale. Il \filename{Dockerfile}\ usato
si basa su due stage. Il primo si occupa di compilare il codice, il secondo invece fornisce
l'immagine di base per l'esecuzione. Il \filename{Dockerfile} viene mostrato di seguito.
\inputminted{dockerfile}{code/07_05_mooncloudcli.dockerfile}
Grazie a questi accorgimenti, l'immagine finale del \mooncloudcontainer\ è di soli $19$ MB.

\noindent \textbf{Pubblicazione} Mettere a disposizione tale immagine per l'utilizzo a terzi è
semplice: è sufficiente pubblicare l'immagine in un registry Docker.
Tale attività viene svolta tramite il \emph{registry} Docker di Gitlab usato per sviluppare Moon Cloud.
Tale \emph{registry} è privato, e come tale richiede autenticazione per poter accedervi. Al momento
dell'iscrizione alla piattaforma Moon Cloud, il \emph{token} di autenticazione viene rilasciato, rendendo
quindi l'immagine accessibile.

\subsection{Il file fornito}\label{subsec:cicd-native-deployment-file}
Gli strumenti DevSecOps messi a disposizione da Gitlab offrono un
file YAML che può essere importato nel file principale della CI/CD.
Allo stesso modo, ne è stato predisposto uno per poter utilizzare Moon Cloud. Tale file,
chiamato \gitlabcimooncloud,
configura il \mooncloudcontainer\ con i parametri di default,
e mette a disposizione un \gitlabjob\ chiamato \gitlabjobmooncloud. Nella definizione di
tale file, si sono sfruttate varie caratteristiche del file \gitlabci, descritte in dettaglio in
Sezione~\ref{subsec:devsecops-gitlab-ci-details-pipeline}.
Il file si compone di diverse sezioni, descritte di seguito.
\begin{description}
    \item[Definizione \gitlabvariables] Si definiscono una serie di variabili di default
    che saranno disponibili nell'esecuzione del \mooncloudcontainer. In particolare, si definisce
    l'URL di base delle API di Moon Cloud, si disabilitano le modalità di \emph{debug} e
    \emph{pretty-print}, e si fornisce una descrizione di default per la \er\ che verrà creata.
    Tutte queste variabili possono essere sovrascritte nel file che lo include.  
    \inputminted[firstline=1, lastline=10]{yaml}{code/07_05_gitlab_ci_mooncloud_secondary.yml} 
    \item[Definizione \gitlabscript] La definizione dei singoli passi da eseguire viene fatta
    mediante una ancora YAML. L'utilizzo di una ancora YAML fa sì che tale definizione possa
    essere richiamata direttamente nel file principale ed inserita in un \gitlabjob\ a scelta,
    nel caso in cui non si voglia usare il \gitlabjob\ predefinito da \gitlabjobmooncloud.
    Nel dettaglio, si svolgono i seguenti passi.
    \begin{enumerate}
        \item Registrazione di un nuovo target, usando il metodo sopracitato per salvarne l'ID in
        una variabile d'ambiente successivamente accessibile (\codeline{18}).
        \item Creazione ed esecuzione di una nuova \er, usando il metodo sopracitato per salvarne
        l'ID in una variabile d'ambiente successivamente accessibile (\codeline{20}).
        \item Ottenimento del risultato dell'\er, usando l'opzione \cmd{--bad-exit-status-if-failed}.
        Per evitare che il fallimento dell'\er\ fermi l'esecuzione, impedendo di eseguire il passo $4)$,
        si salva il codice d'uscita del comando in una variabile
        (\cmd{exit\_code=\$?})~\footnote{Il codice d'uscita dell'ultimo comando eseguito, nella shell \emph{ash} usata da Alpine Linux,
        è disponibile nella variabile \envvar{\$?}. Per evitare di perdere il valore di tale variabile nell'esecuzione
        del successivo comando, il contenuto della variabile viene copiato in un'altra variabile, chiamata
        \envvar{exit\_code}.}. Tale codice d'uscita sarà valutato successivamente mediante la variabile \envvar{exit\_code}.
        \item Cancellazione della \er\ appena eseguita, e del target (\codelines{27}{28}).
        \item Valutazione del codice d'uscita precedentemente salvato, identificante il successo o meno
        dell'esecuzione dell'\er. Se il codice d'uscita è diverso da 0, allora l'evaluation è fallita,
        quindi si termina l'esecuzione con il comando \cmd{exit 1}. Tale comando fa ritornare al chiamante
        con il codice d'errore specificato, ovvero $1$. In questo modo si può fermare l'esecuzione dei passi
        successivi, poiché l'attività di assurance non è andata a buon fine (\codeline{31}). 
    \end{enumerate}
    \inputminted[firstline=12, lastline=31]{yaml}{code/07_05_gitlab_ci_mooncloud_secondary.yml}
    \item[Definizione \gitlabjob\ \gitlabjobmooncloud] Si definisce un \gitlabjob\ chiamato \gitlabjobmooncloud,
    specificando che appartiene allo \gitlabstage\ di \giltabstagename{test} (\codelines{34}{36}).
    Mediante la direttiva \gitlabimagett, si specifica che il \gitlabjob\ in questione deve essere eseguito
    nel \mooncloudcontainer, di cui si specifica il nome (\codeline{38}).
    Infine, si specificano i comandi, ovvero gli \gitlabscript, da eseguire dentro questo \gitlabjob.
    Si sfruttano le ancore YAML per richiamare la sequenza di comandi definiti precedentemente (\codelines{40}{41}).
    \inputminted[firstline=33, lastline=41]{yaml}{code/07_05_gitlab_ci_mooncloud_secondary.yml} 
\end{description}
Nel complesso, il file si presenta come segue.
\inputminted{yaml}{code/07_05_gitlab_ci_mooncloud_secondary.yml}

\subsection{Uso}\label{subsec:cicd-native-deployment-use}
Il file \gitlabcimooncloud\ fornisce una serie di parametri di default
ed un \gitlabjob\ pronto all'uso. Per poter essere effettivamente usato,
deve essere incluso nel file di CI/CD principale.
In particolare, oltre ad importare il file mediante la direttiva \key{include}, il team DevOps
deve specificare i parametri d'uso. I parametri che devono essere definiti, sotto forma di
variabili d'ambiente, sono i seguenti. Essendo valori dipendenti dal contesto d'uso,
non si fornisce nessun default.

\begin{description}
    \item[\envvar{MOONCLOUD\_SDK\_TARGET\_NAME}] Questa variabile d'ambiente contiene il nome da assegnare
    al target che viene creato.
    \item[\envvar{MOONCLOUD\_SDK\_PROJECT\_ID} e \envvar{MOONCLOUD\_SDK\_ZONE\_ID}] Queste due variabili d'ambiente
     contengono l'ID del \mooncloudproject\ e della \mooncloudzone, rispettivamente, in
    cui il target creato verrà inserito. \mooncloudproject\ e \mooncloudzone\ sono due \emph{contenitori}
    logici entro cui un target viene inserito. Si assume che questi due oggetti siano stati creati
    tramite la \moonccomp{Dashboard}, o comunque al momento della registrazione del team DevOps in Moon Cloud.
    \item[\envvar{MOONCLOUD\_SDK\_TARGET\_TYPE} e \envvar{MOONCLOUD\_SDK\_TARGET\_HOSTNAME}] Queste due
    variabili d'ambiente contengono il \emph{tipo} di target che viene registrato (\emph{host, url, azure, aws, artifact, ecc\ldots}),
    e l'hostname. Si noti che l'hostname si utilizza solo quando il target non è un \emph{artifact}.
    \item[\envvar{MOONCLOUD\_SDK\_AER\_ID}] Questa variabile d'ambiente contiene l'ID della
    \aer\ da istanziare creando ed eseguendo la \er. 
    \item[\envvar{MOONCLOUD\_SDK\_UER\_NAME}] Questa variabile d'ambiente contiene il nome
    da assegnare alla \er\ che viene creata.
    \item[\envvar{MOONCLOUD\_SDK\_UER\_CONTROLS}] Questa variabile d'ambiente contiene il
    \emph{payload} effettivo che viene passato in input ai controlli della \er. In concreto, contiene
    le opzioni di esecuzione di ciascun controllo. Tale variabile d'ambiente deve essere formatta come
    un array JSON di oggetti, in cui ciascun oggetto contiene l'ID del controllo ed il suo input.
    Si noti che per ottenere l'ID dei vari controlli, il team DevOps può utilizzare le API di Moon Cloud
    o consultare la documentazione fornita.
    Il software fornisce anche il supporto alla lettura di file automatizzata, specificando il percorso del
    file da leggere. In dettaglio, se un valore dell'array JSON è racchiuso tra doppie parentesi angolari, il
    software tratta ciò che è racchiuso tra esse come il nome di un file. Il software legge tale file,
    e sostituisce al valore il contenuto del file.
    Ad esempio, supponendo che il valore della variabile \envvar{MOONCLOUD\_SDK\_UER\_CONTROLS} sia
    \mintinline{json}{[{"id": 2, "data": {"config": {"file": "<<Makefile>>"}}}]}, il software
    rileva le doppie parentesi angolari, e sostituisce a \cmd{<<Makefile>>} il contenuto del file stesso.
    \item[\envvar{MOONCLOUD\_SDK\_USERNAME} e \envvar{MOONCLOUD\_SDK\_PASSWORD}] Queste variabili d'ambiente\\
    contengono le credenziali per autenticarsi con le API di Moon Cloud. Si suppone che siano gestite in maniera
    \emph{sicura}, e non scritte direttamente nel file.
\end{description}

Un ipotetico file \gitlabci, in versione semplificata, che utilizzi Moon Cloud, si presenta come segue.
Le parti inerenti l'uso di Moon Cloud sono evidenziate.
In dettaglio, la pipeline è composta da due \gitlabstage, chiamati \giltabstagename{build}\ e \giltabstagename{staging}.
Il primo di essi è composto da un solo \gitlabjob\ chiamato \gitlabjobname{compile}, che compila il progetto (\codelines{8}{15}).
Il secondo \gitlabstage, invece, è composto da due \gitlabjob. Il primo, \gitlabjobname{pre-deploy},
esegue il software compilato al punto precedente, in un deploy di \emph{staging} (\codelines{20}{28}).
Il secondo \gitlabjob\ si chiama \gitlabjobmooncloud, ed è preso dal file importato. Grazie alla funzione
di merge dei file offerta da Gitlab (Sezione~\ref{subsec:devsecops-gitlab-ci-details-pipeline}), i vari parametri
del \gitlabjob\ \gitlabjobmooncloud\ sono ereditati dal file importato, e si vanno solo a definire
le variabili d'ambiente necessarie alla configurazione del \gitlabjob\ in questione.
\inputminted[highlightlines={30-51}]{yaml}{code/07_05_gitlab_ci_mooncloud_main.yml}
