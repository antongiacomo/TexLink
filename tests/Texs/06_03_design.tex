\section{Progettazione}\label{sec:cicd-native-design}

\subsection{Scelta dell'SDK}\label{subsec:cicd-native-design-sdk-choice}
Per poter rispettare i requisiti indicati, si è deciso di realizzare il \mooncloudcontainer\
usando l'SDK scritto nel linguaggio di programmazione Go.
Infatti, l'eseguibile prodotto dal compilatore Go è statico e non necessita di nessuna dipendenza runtime,
poiché tutto ciò che serve per l'esecuzione si trova già nell'eseguibile, consentendo di
avere un'immagine finale molto leggera.
Se fosse stato scelto un SDK basato su Java o Python, sarebbe stato necessario
aggiungere al \mooncloudcontainer\ il runtime di tali linguaggi, ovvero la JVM
e l'interprete Python, rispettivamente.
Lo stesso discorso vale anche per Typescript, che viene compilato in Javascript e necessita
di NodeJS per l'interpretazione. Ciò ha escluso dalla scelta dei linguaggi possibili Java,
Python e Typescript.

Non è stato scelto nemmeno Rust, sebbene sia stato usato con successo nel progetto \ciexecutor\ (Capitolo~\ref{trustworthy}).
Questo perché il codice generato è basato su un paradigma di I/O sincrono che nel frattempo
è cambiato. Si è preferito quindi non utilizzare un codice non allineato con ciò
che si scriverebbe a mano.

Non è scelto nemmeno Kotlin, poiché nessuno del team di Moon Cloud ha esperienza con tale
linguaggio. Per quanto Go non sia usato in altre parti di Moon Cloud, esso è già noto
al team Moon Cloud, e si è sfruttato il progetto del \mooncloudcontainer\ per sperimentare
con un nuovo linguaggio di programmazione. Inoltre, grazie alla sua semplicità,
Go è estremamente facile da imparare.

\subsection{Architettura}\label{subsec:cicd-native-design-architecture}
Il software da inserire all'interno del \mooncloudcontainer\ viene realizzato come
una CLI (\emph{Command Line Interface}), cioè un programma a linea di comando. Le varie
opzioni passate al programma al momento dell'esecuzione definiscono l'azione da fare ed
i relativi parametri, come creazione di target, esecuzione di una valutazione, ecc\ldots
I requisiti hanno dettato le seguenti scelte architetturali.

\begin{description}
    \item[Configurazione con variabili d'ambiente] Per poter soddisfare il requisito
    di \emph{configurazione con variabili d'ambiente} occorre che i parametri delle
    varie azioni debbano essere specificati mediante variabili d'ambiente, oltre che tramite CLI.
    È necessario, quindi, progettare un software che possa gestire sia
    configurazioni ricevute dalla CLI, sia da variabili d'ambiente.
    \item[Astrazione] Per poter soddisfare il requisito di \emph{astrazione} occorre che
    il software sia sufficientemente evoluto, e non un semplice layer al di sopra dell'SDK generato.
    Si consideri l'esecuzione di una valutazione. Per poter lanciare una valutazione, è
    necessario compilare un \emph{payload} relativamente complesso, in cui occorre, ad esempio,
    specificare i nomi dei controlli che compongono l'\aer\ che si vuole istanziare.
    Tuttavia, poiché l'elenco dei controlli di ciascuna \aer\ è disponibile interrogando l'API
    di ottenimento dei dettagli di quella \aer, si vuole evitare che gli utenti specifichino
    tale elenco. Per compilare il \emph{payload} richiesto e fornire quindi un'astrazione maggiore,
    il programma dovrebbe in automatico specificare i nomi dei controlli, ottenendoli
    dall'API, richiedendo al team DevOps di specificare solo i parametri strettamente
    necessari all'esecuzione della valutazione.
    \item[Configurazione minimale] Per poter soddisfare il requisito di \emph{configurazione minimale},
    in combinazione con quello di \emph{configurazione con variabili d'ambiente}, occorre predisporre
    un sistema di gestione della configurazione avanzato. Per facilitare la realizzazione del
    software, tale sistema deve consentire di: $i)$ recuperare parametri indipendentemente che
    siano stati specificati tramite CLI o variabili d'ambiente e $ii)$, supportare
    la definizione di valori di default.
    \item[Immagine minimale] Per poter soddisfare il requisito di \emph{immagine minimale}
    occorre utilizzare un linguaggio di programmazione che non necessita di un runtime,
    la scelta di Go rispetta questo requisito. Inoltre, visto che il \mooncloudcontainer\ è
    un container, occorre utilizzare una \emph{multi-stage build} per limitarne ulteriormente
    la dimensione. 
\end{description}

\subsection{Funzionalità}\label{subsec:cicd-native-design-functionalities}
Il software, chiamato semplicemente \mooncloudcli, offre le seguenti funzionalità.

\begin{description}
    \item[Target] È possibile registrare un nuovo target, listare tutti i target di un certo
    utente, ottenere i dettagli di un certo target, e cancellare un certo target.
    Per farlo, vengono messi a disposizione i seguenti comandi.
    \begin{itemize}
        \item \cmd{mooncloud target create [opzioni]}\ per creare un target usando le
        varie \cmd{opzioni}.
        \item \cmd{mooncloud target list}\ per listare tutti i target registrati dall'utente.
        \item \cmd{mooncloud target get <TARGET-ID>}\ per ottenere i dettagli del target
        identificato da \cmd{TARGET-ID}. Tale ID può essere specificato anche come variabile
        d'ambiente.
        \item \cmd{mooncloud target delete <TARGET-ID>}\ per cancellare il target identificato
        da \cmd{TARGET-ID}. Tale ID può essere specificato anche come variabile
        d'ambiente.
    \end{itemize}
    \item[Controlli] È possibile listare i controlli disponibili, ed ottenere i dettagli
    di un certo controllo. Per farlo, vengono messi a disposizione i seguenti comandi.
    \begin{itemize}
        \item \cmd{mooncloud control list}\ per listare tutti i controlli disponibili.
        \item \cmd{mooncloud control get <CONTROL-ID>}\ per ottenere i dettagli del controllo
        identificato da \cmd{CONTROL-ID}.
    \end{itemize} 
    \item[\aer] È possibile listare le \aer\ disponibili, ed ottenere i dettagli di una
    certa \aer. Per farlo, vengono messi a disposizione i seguenti comandi.
    \begin{itemize}
        \item \cmd{mooncloud aer list}\ per listare tutte le \aer\ disponibili.
        \item \cmd{mooncloud aer get <AER-ID>}\ per ottenere i dettagli della \aer\ 
        identificata da \cmd{AER-ID}.
    \end{itemize} 
    \item[\er] È possibile creare ed eseguire una nuova \er, listare tutte le \er\ di un
    certo utente, ottenere i dettagli di una certa \er, e cancellare una \er.
    Per farlo, vengono messi a disposizione i seguenti comandi.
    \begin{itemize}
        \item \cmd{mooncloud uer create [opzioni]}\ per creare ed eseguire una \er\ usando
        le varie \cmd{opzioni}
        \item \cmd{mooncloud uer list}\ per listare tutte le \er\ create dall'utente.
        \item \cmd{mooncloud uer get <UER-ID>}\ per ottenere i dettagli della \er\ identificata
        da \cmd{UER-ID}. Viene messa a disposizione l'opzione \cmd{--bad-exit-status-if-failed},
        che, nel caso in cui la \er\ sia terminata con errore, ritorna come codice d'uscita
        al sistema operativo un codice d'errore. Ciò fa sì che, quando questo comando sia eseguito
        in una pipeline, blocchi l'esecuzione delle fasi successive. 
        Tale ID può essere specificato anche come variabile d'ambiente. Inoltre, l'opzione \cmd{--wait-for-end}\ 
        fa sì che il client continui a chiamare la API sottostante fino a ché l'esecuzione dell'\er\ in questione
        non è terminata.
        \item \cmd{mooncloud uer delete <UER-ID>}\ per cancellare la \er\ identificata da
        \cmd{UER-ID}. Tale ID può essere specificato anche come variabile
        d'ambiente.
    \end{itemize}
\end{description}

\noindent \textbf{Gestione degli ID} Un problema che si è dovuto risolvere è la necessità di conoscere l'ID numerico
che identifica gli oggetti con cui si interagisce. Nel momento in cui si crea un target, per esempio,
\mooncloudcli\ stampa su standard output la risposta di Moon Cloud, contenente, tra le varie informazioni,
l'ID del target appena creato.
Nell'usare \mooncloudcli\ in maniera interattiva, l'utente può ispezionare l'output, e quindi
specificare l'ID per le successive invocazioni del comando.
Tuttavia, nell'inserire \mooncloudcli\ in una pipeline, non è possibile che l'utente veda l'output,
perché tutto deve avvenire in maniera automatizzata.
Per risolvere questo problema si è proposta la seguente soluzione. I comandi per le creazione
di target e \er\ prevedono l'opzione \cmd{--write-id-to <filename>}. Se tale opzione viene
specificata, \mooncloudcli\ scrive su \cmd{filename}\ l'ID dell'oggetto creato, sotto forma di
un comando di shell per la definizione di una variabile d'ambiente. La variabile d'ambiente
che viene scritta ha lo stesso nome della variabile d'ambiente che viene utilizzata dai
sotto-comandi che necessitano un ID in input.
Si considerino i sotto-comandi che gestiscono i target. Eseguendo il comando
\cmd{mooncloud target create [opzioni] --write-id-to ids.sh}, supponendo che l'ID
del target creato sia $34$, il contenuto del file \filename{ids.sh} è come segue.
\begin{minted}{bash}
export MOONCLOUD_SDK_TARGET_ID=34
export MOONCLOUD_SDK_UER_TARGET_ID=34
\end{minted}
La variabile \envvar{MOONCLOUD\_SDK\_TARGET\_ID} ha lo stesso nome della variabile d'ambiente
usata dai comandi \cmd{mooncloud target get}\ e \cmd{mooncloud target delete}.
La variabile \envvar{MOONCLOUD\_SDK\_UER\_TARGET\_D}, invece, ha lo stesso nome della variabile d'ambiente
usata nel comando \cmd{mooncloud uer create} per specificare il target della valutazione.
Per far sì che tale variabile d'ambiente venga effettivamente settata, occorre
usare il comando \cmd{source ids.sh}, che \emph{importa} le variabili d'ambiente.
Grazie a ciò, è possibile usare \cmd{mooncloud target get}\ direttamente,
poiché il software usa come ID quello letto dalla variabile d'ambiente \envvar{MOONCLOUD\_SDK\_TARGET\_ID}.
Complessivamente, si digitano i comandi che seguono.
\begin{minted}{bash}
# 1. Creating the target
# other creation options omitted
mooncloud target create --write-to-id ids.sh
# 2. Making the environment variable. The `source <file>` command
# is a shell command that reads and execute the code written into <file>.
# In this case the code written into <file> indicates to create two environment
# variables, holding the ID of the created target.
source ids.sh
# 3. Getting the target whose ID has been written into ids.sh
mooncloud target get
\end{minted}
