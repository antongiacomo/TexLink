\section{I controlli usati}\label{sec:case-study-controls-used}

\subsection{Il controllo esterno}\label{subsec:case-study-controls-used-external}
Il controllo esterno da integrare, e la relativa \aer, sono chiamati entrambi \probeservicetime.
Esso appartiene alla categoria delle \emph{meta-probe}, ovvero controlli responsabili di verificare
\emph{meta-informazioni}. Lo scopo del controllo in questione è quello di valutare il tempo di
risposta di un servizio HTTP, mediante una chiamata \httpget.

La probe \probeservicetime\ riceve in input la URL verso cui occorre misurare il tempo di esecuzione,
ed il tempo massimo di esecuzione, entro cui la probe viene ritenuta conclusa con successo.
L'unica evidenza raccolta è il tempo di esecuzione della chiamata HTTP, che viene confrontato con
il tempo massimo, ed usato per determinare il risultato Booleano.

Il nucleo del controllo è un piccolo software scritto in Rust, che effettua una chiamata \httpget\ alla
URL passatagli in input, e ne misura il tempo di esecuzione. La scelta del linguaggio Rust non è casuale.
Infatti, grazie alle sue elevate performance, si presta ad effettuare una misurazione di questo tipo.
In particolare, essendo un linguaggio compilato con assenza di runtime, garantisce che le misurazioni
effettuate dipendano solo dalla latenza di rete, e non dalle performance del linguaggio.
Il software produce un output in JSON su standard output, contenente il tempo di esecuzione.
Esso è utilizzato nella probe come un processo esterno, che viene lanciato dal codice
Python.
I tempi vengono misurati direttamente nel software Rust, e non nella codice della probe che lo richiama.
Ciò è necessario perché, se le misurazioni fossero effettuate dal codice Python, esse sarebbero
influenzate anche dal tempo di esecuzione del processo esterno.
Il codice Rust, usato nella probe, è molto semplice, ne viene mostrato di seguito un estratto.
\inputminted[highlightlines={21}]{rust}{code/09_02_service_response_time.rs}

Il codice Python della probe è altresì semplice. Come prima cosa, esso si occupa di controllare che l'input,
cioè la URL ed il tempo massimo, sia corretto. In seguito, esegue il comando esterno, il quale effettua la chiamata
HTTP e ne misura il tempo di esecuzione. Poi, ottiene il risultato dell'esecuzione del comando, ovvero
il tempo di esecuzione della chiamata HTTP; questa fase è facilitata dal fatto che il comando Rust produca un output JSON.
Infine, controlla che il tempo massimo, ricevuto in input, sia inferiore al tempo di esecuzione della chiamata HTTP.
Una versione semplificata viene mostrata di seguito.
\inputminted[highlightlines={26-33, 41-45}]{python}{code/09_02_service_response_time.py}

Un input valido per la probe in questione è il seguente. Si specifica che il target dell'analisi è
\url{https://google.it}, e che il tempo massimo, espresso in millisecondi, è $300$.
\inputminted{json}{code/09_02_service_response_time_input.json}
Un possibile output per tale input è il seguente. Il risultato Booleano è \true\ (\key{status}).
Gli \extradata\ (\key{data}) mostrano il temp di esecuzione.
\inputminted[highlightlines={2}]{json}{code/09_02_service_response_time_output.json}

\noindent \textbf{La pipeline} La probe \probeservicetime\ è sviluppata in CI/CD usando Gitlab, e presenta
una pipeline simile a quella dei controlli ufficiali di Moon Cloud. Gli step che vengono svolti sono i seguenti.
Come prima cosa, si effettua una \emph{build} dell'immagine del controllo, specificando come
\emph{tag} un'immagine di test. Successivamente, si effettua dello unit testing, nel
\gitlabjob\ chiamato \gitlabjobname{test}. L'ultimo passo consiste nel rilasciare
l'immagine. L'ultimo \gitlabjob, \gitlabjobname{deploy}, viene eseguito solo se la fase di unit testing va a buon fine.
Il file \gitlabci\ si presenta come segue.
\inputminted{yaml}{code/09_02_service_response_time_old_gitlabci.yml}

\subsection{Il controllo di Moon Cloud}\label{subsec:case-study-controls-used-internal}
L'attività di assurance effettuata da Moon Cloud per il controllo \probeservicetime\ sfrutta
una \aer\ per la scansione delle dipendenze. Tale \aer, chiamata \aerrustcheck\ e composta da un unico controllo,
effettua la scansione delle dipendenze di un progetto Rust, certificando che non vi siano
vulnerabilità, o altri problemi, nelle librerie utilizzate.
La ragione per cui tale \aer\ è stata scelta è che la scansione delle dipendenze per Rust non
è supportata da Gitlab. Inoltre, l'approccio alla sicurezza nella pipeline di Moon Cloud, rispetto
a quello di Gitlab, presenta sempre la stessa interfaccia. Infatti, ciò che cambia nell'eseguire
una \aer\ rispetto ad un'altra è la configurazione mediante variabili d'ambiente.

\emph{Cargo}, il package manager utilizzato per la gestione delle dipendenze Rust,
offre un plugin per la scansione delle dipendenze, chiamato \emph{cargo-audit}~\cite{cargo-audit}.
Per funzionare, si collega ad un database mantenuto dalla comunità~\cite{rustsec}, ed è in grado di riportare
non solo vulnerabilità note per le dipendenze usate, ma anche ulteriori problemi,
come l'utilizzo di librerie non mantenute.
Per funzionare, \emph{cargo-audit}, legge i file \filename{Cargo.toml}\ e \filename{Cargo.lock},
i due file utilizzati da \emph{cargo} per gestire le dipendenze di un progetto Rust.

Il controllo dell'\aer\ \aerrustcheck\ si basa su \emph{cargo-audit} per la propria esecuzione.
Come prima cosa, esso riceve in input il contenuto dei file \filename{Cargo.toml}\ e \filename{Cargo.lock},
ed il nome del progetto Rust.
In seguito, esegue i comandi per preparare un progetto Rust sfruttando i due file ricevuti in input.
Ciò va fatto perché il comando \emph{cargo-audit} può essere eseguito solo dentro
una directory che rispetta la struttura definita per i progetti Rust.
In seguito, esegue il comando \cmd{cargo audit}, e ne cattura l'output, in formato JSON. Infine,
il controllo decide il risultato Booleano dell'esecuzione, basandosi sull'output di \cmd{cargo audit}.
Il risultato è \true\ solo se non vi sono vulnerabilità o \emph{warning}.

Una versione semplificata del codice della probe in questione è il seguente.
\inputminted[highlightlines={34-43, 49-56, 64-68}]{python}{code/09_02_cargo_audit_probe.py}
Un input valido per la probe è composto dal contenuto dei file \filename{Cargo.toml}\ e \filename{Cargo.lock},
essendo particolarmente verboso e lungo, non ne viene mostrato un esempio.
Un possibile output, di cui si riportano solo le parti più importanti, è il seguente.
In questo caso, il risultato è \false, poiché è stato trovato un problema, relativo ad una libreria
\emph{yanked}, ovvero la cui pubblicazione nel \emph{registry} di librerie Rust è stata annullata~\cite{cargo-yank}.
\inputminted[highlightlines={4, 9, 11}]{json}{code/09_02_cargo_audit_probe_output.json}
