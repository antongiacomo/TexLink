\section{Possibili realizzazioni}\label{sec:cert-controls-triggered-possible-realization}
I controlli sono le unità di più basso livello usate da Moon Cloud nel processo di valutazione,
tuttavia la granularità con la quale gli utenti finali, compresi i team DevOps, hanno a che fare
è quella di \aer. Per tale ragione, il soddisfacimento del requisito di \certcontrolstrigg\ deve essere basato
sull'aggiornamento di controlli delle \aer\ usate dagli utenti.

Esso può essere realizzato in almeno due modi differenti: \subscription\ e \notification,
descritti di seguito.

\subsection{\emph{Subscription}}\label{subsec:cert-controls-triggered-possible-realization-subscription}
Il meccanismo di \subscription\ si basa sulla registrazione, all'interno
del framework di assurance, delle diverse pipeline implementate dai team DevOps. I team
DevOps, in particolare, registrano all'interno di Moon Cloud tutte le pipeline che utilizzano
per la loro esecuzione Moon Cloud. Nelle informazioni che vengono registrate, il team
specifica anche quali \aer\ vengono utilizzate in ciascuna pipeline.
Un componente interno di Moon Cloud sviluppato ad hoc è demandato ad innescare l'esecuzione delle varie pipeline,
usando le API del sistema di CI/CD.
Tale esecuzione, a sua volta, viene innescata dal processo CI/CD dei controlli.
Nel complesso, l'attività potrebbe svolgersi come segue.

\begin{enumerate}
    \item Il team DevOps registra in Moon Cloud la propria pipeline, specificando quali \aer\ vengono
    utilizzate.
    \item Un controllo viene aggiornato, la pipeline CI/CD del controllo fa partire il componente
    demandato all'esecuzione delle pipeline.
    \item Il componente riceve in input i dettagli del controllo che è stato aggiornato,
    e verifica a quali \aer\ appartiene.
    \item Il componente verifica poi quali pipeline utilizzano le \aer\ il cui controllo è stato aggiornato e,
    per ciascuna di esse, ne lancia l'esecuzione da remoto, sfruttando le API del framework CI/CD
    usato da quella pipeline.
\end{enumerate}

Il meccanismo di \subscription\ presenta il seguente vantaggio: semplicità lato client. 
Tutto ciò che il team DevOps deve fare per utilizzar questo meccanismo è registrare le proprie pipeline in
Moon Cloud. Vi sono, tuttavia, due svantaggi principali ad un approccio del genere.
\begin{itemize}
    \item Supporto lato client. Il framework CI/CD usato dal team DevOps deve supportare il lancio di
    pipeline da remoto, mediante delle API interrogabili da Moon Cloud.
    \item Complessità lato Moon Cloud. Si richiede innanzitutto la modifica della pipeline CI/CD dei
    controlli, in modo che ogni aggiornamento invii delle notifiche al nuovo componente, anch'esso
    da scrivere. Il componente deve anche supportare l'esecuzione di pipeline per diversi framework.
    \item Autenticazione. Le API per l'esecuzione di pipeline da remoto, se supportate, non possono che
    richiedere autenticazione per poter essere usate. Occorre quindi che Moon Cloud, in qualche modo,
    memorizzi tali credenziali in maniera sicura.
\end{itemize} 

\subsection{\emph{Polling}}\label{subsec:cert-controls-triggered-possible-realization-notification}
Questo meccanismo cerca di suddividere la complessità tra il lato client ed il server, anziché aver tutta
la complessità lato Moon Cloud.
Si basa sulla realizzazione di una API che i client possono interrogare per verificare se un \aer\ è
stata aggiornata o meno. Sulla base del risultato ritornato dalla API, il client può far partire la
pipeline o meno. In dettaglio, l'API riceve in input l'ID di una \aer, e ritorna in output
un array contenente i \emph{digest} di ciascuna immagine dei controlli coinvolti nella \aer~\footnote{Il
\emph{digest} è un hash, che viene utilizzato da Docker per identificare univocamente
una immagine ad una certa versione.}.
Nel dettaglio, il meccanismo funziona come segue.
\begin{enumerate}
    \item Lato client, quando si esegue una pipeline che coinvolge Moon Cloud, si chiama
    l'API, che ritorna il \emph{digest} che identifica la versione attuale dei controlli usati
    per l'esecuzione. Tale \emph{digest} viene memorizzato in qualche modo.
    \item Periodicamente, lato client si chiama l'API, ottenendo i nuovi \emph{digest}.
    \item Si confrontano i \emph{digest} ottenuti con quelli precedentemente salvati,
    se sono diversi, allora si fa ripartire la pipeline.
\end{enumerate}

Questo approccio presenta i seguenti vantaggi.
\begin{itemize}
    \item Divisione della complessità. La complessità viene suddivisa tra Moon Cloud ed i client. Lato
    Moon Cloud occorre realizzare una nuova API REST; lato client uno script che interroghi tale
    API e successivamente esegua la pipeline, se necessario. Nessuna delle due attività, prese singolarmente,
    è troppo complessa. Ad esempio, l'esecuzione periodica dello script lato client può essere realizzata
    mediante l'utility di Linux \emph{cron}.
    \item Scelta lato client. L'esecuzione effettiva è governata dallo script lato client, quindi
    esso può decidere se effettivamente far ripartire la pipeline, e se sì può comunque prorogarne
    l'esecuzione, ad esempio, se gli esecutori della pipeline in quel momento sono particolarmente
    carichi di lavoro.
\end{itemize}

Dall'altro lato, lo svantaggio principale sta nel portare parte della complessità lato client, per quanto
minima essa sia. Inoltre, con il meccanismo di \subscription, l'esecuzione delle pipeline avviene immediatamente
in seguito agli aggiornamenti, garantendo che le verifiche di sicurezza siano sempre allineate.

Considerati vantaggi e svantaggi, si è scelto di soddisfare il requisito di \certcontrolstrigg\ mediante
il meccanismo di \notification.
