\section{Prerequisiti}\label{sec:api-driven-pre-req}

\subsection{REST}\label{subsec:api-driven-pre-req-rest}
REST è un paradigma per la programmazione web, definito per la prima volta all'inizio
degli anni 2000~\cite{fielding2000architectural}.  REST, rispetto ad altri paradigmi come SOAP,
predilige un approccio leggero, basato su un uso \emph{puro} di HTTP.
I due concetti fondamentali di REST sono quelli di \restresource\ e di \restoperations\ fatte su essa.

\noindent \textbf{Risorsa} Una \restresource\  è identificata da una URL all'interno di un server. Ad esempio, senza considerare
la parte relativa all'host, la URL \url{/users/johndoe/} identifica l'utente \emph{johndoe}, come
una \restresource\ \emph{gerarchicamente figlia} di \emph{users}. Spesso si utilizza un ID
numerico (o alfanumerico) per identificare una \restresource\ (ad esempio \url{/users/10/}).

\noindent \textbf{Operazione} Su ciascuna \restresource\ è possibile fare delle \restoperations, tipicamente la creazione di una \restresource,
il suo aggiornamento, l'ottenimento di dettagli relativi alla \restresource\ stessa e la sua cancellazione.
Una \restoperation\ su una \restresource\ è definita dal metodo HTTP usato per richiedere quella \restresource\ al web
server, secondo lo schema che segue.

\begin{description}
    \item[\httppost] Viene utilizzato per creare una \restresource. Il \emph{body} della richiesta specifica
    tutti i parametri richiesti dal server per la creazione; solitamente nella risposta il server
    ritorna l'ID numerico con cui riferirsi alla \restresource.
    Un esempio di URL per una richiesta \httppost\ è il seguente: \url{/users/}.
    \item[\httpget] Viene utilizzato per ottenere una \restresource. Una richiesta \httpget\ è per
    definizione idempotente, e non ha un \emph{body}. Rispetto alla richiesta \httppost,
    la URL avrà come ultimo parametro l'ID della \restresource\  come ritornato
    dalla \httppost, un esempio è il seguente: \url{/users/10/}.
    \item[\httpput\ e \httppatch] Vengono utilizzati per aggiornare la \restresource. Il \emph{body} della richiesta
    specifica alcuni \emph{campi} della \restresource\ che devono essere aggiornati. Come per la
    richiesta \httpget, la URL avrà come ultimo parametro l'ID della \restresource.
    \item[\httpdelete] Viene utilizzo per cancellare una \restresource, usando la stessa URL delle richieste
    \httpget, \httpput, \httppatch.
\end{description}

% Dal punto di vista del paradigma architetturale cosa in concreto significhi \emph{creare una risorsa}
% non è definito, poiché REST si limita a definire l'interfaccia del servizio.
% Si può pensare che la creazione comporti degli inserimenti in una base di dati, e che la cancellazione
% dello stessa comporti delle cancellazione sul database utilizzato dal servizio dietro le quinte.
% Il metodo HTTP in REST quindi definisce la \emph{semantica} dell'operazione.

Altri concetti importanti sono quelli di \emph{content type}, \emph{status code} e \emph{servizio REST}. 

\noindent \textbf{\emph{Content type}} Le risorse utilizzate in REST sono delle risorse \emph{astratte},
tuttavia client e server devono accordarsi sul formato di interscambio utilizzato per descrivere
tali \restresources. Tipicamente si utilizza il formato JSON, nel caso in cui si supportino diversi
formati, client e server possono accordarsi sul formato preferito. Per tale scopo, è possibile
usare header HTTP, come \emph{Accept} e
\emph{Content-type}~\cite{mdn-header-accept, mdn-header-content-negotiation, mdn-header-content-type}.

\noindent \textbf{\emph{Status code}} Un altro componente importante per un'interazione REST sono gli \emph{status code}, che, come in altri
protocolli applicativi, ad esempio FTP, vengono usati per indicare il successo o meno della richiesta fatta dal client.
Come per i metodi HTTP, anche per gli \emph{status code} ci si affida alla loro semantica originale, come definito
nel protocollo HTTP. Tra gli \emph{status code} più usati vi sono i seguenti.

\begin{description}
    \item[\httpstatus{200}] Generico \emph{status code} indicante successo, viene utilizzato
    tutte le volte che l'\restoperation\ sulla \restresource\ viene eseguita con successo e non vi sono
    \emph{status code} più specifici da usare. In particolare viene utilizzato nelle \httpget,
    \httpput\ e \httppatch.
    \item[\httpstatus{201}] \emph{Status code} indicante successo, viene utilizzato
    in risposta a richieste di creazione di \restresources. 
    \item[\httpstatus{204}] \emph{Status code} indicante successo, viene utilizzato tutte
    le volte che l'\restoperation\ sulla \restresource\ viene eseguita con successo e la risposta
    ha un \emph{body vuoto}. In particolare viene utilizzato nelle \httpdelete.
    \item[\httpstatus{400}] \emph{Status code} indicante errore, in particolare che la
    richiesta è non conforme rispetto a come il server se la aspetta (\emph{errore di sintassi}). 
    \item[\httpstatus{404}] \emph{Status code} indicante errore, in particolare che
    la \restresource\ richiesta non esiste. 
    \item[\httpstatus{500}] \emph{Status code} di errore generico, usato quando
    si verifica una condizione d'errore all'interno del server durante il processamento
    della richiesta. 
\end{description}

\noindent \textbf{Servizio REST} Un \emph{Servizio REST}, o \emph{servizio di API REST}, o \emph{API REST},
o \emph{RESTful API} rappresenta l'insieme delle \restresources\ e delle \restoperations\ che sono offerte da un web server
che aderisce al paradigma REST.

\vspace{0.5em}

\noindent \textbf{Discussione} REST è uno dei paradigmi possibili per la programmazione e l'uso di servizi. In contrasto con
approcci più \emph{pesanti}, come SOAP o RPC, REST si caratterizza per una maggiore semplicità.
Infatti, è tipicamente molto più semplice per un client invocare un servizio REST, piuttosto che un servizio
RPC, è infatti sufficiente effettuare una chiamata HTTP.
Quindi, per realizzare un software che utilizza un servizio REST, è sufficiente utilizzare una libreria
per HTTP, che sono disponibili virtualmente per ogni linguaggio di programmazione.
Grazie a ciò, REST è diventato lo standard de facto per realizzazione di applicazioni web
lato server~\cite{10.1145/3340433.3342822}.
Di contro, REST non è uno standard, piuttosto uno stile architetturale, un paradigma per la realizzazione di
servizi. Ciò implica che vi possono essere delle variazioni nel \emph{come} la filosofia REST venga intesa.
Ad esempio, sebbene vi siano delle chiare tendenze, non vi è un consenso generale sulla scrittura
delle URL per definire le risorse.

\subsection{OpenAPI}\label{subsec:api-driven-pre-req-openapi}
OpenAPI è un linguaggio di specifica per API REST. Il rapido diffondersi dello stile REST
nello sviluppo di servizi web ha fatto emergere una pressante necessità: quella della documentazione
delle proprie API. Documentare le API offerte dal proprio servizio è fondamentale se si vuole
che qualcuno lo utilizzi: se le API esposte non sono ben definite, sarà molto difficile utilizzarlo.
Questo bisogno è accentuato dal fatto che REST stesso non è uno standard specifico, quanto più
uno stile.

Sono emersi tentativi per creare documentazione in un formato standard di API REST,
tra cui \emph{API Blueprint}~\cite{api-bluebrint}, \emph{RAML}~\cite{raml} e \emph{Swagger}~\cite{swagger-2}.
Ciascuno con il proprio approccio, alla fine lo standard emerso è stato quello
di \emph{Swagger}, divenuto poi noto con il nome di \emph{OpenAPI v2}. È stata poi pubblicata
una revisione dello standard, nota come \emph{OpenAPI v3}~\cite{openapi2014openapi}.

Lo standard OpenAPI è definito \emph{specifica OpenAPI}, mentre la descrizione di un particolare
servizio REST usando OpenAPI è detta \emph{documento OpenAPI}~\cite{karlsson2019quickrest}.
Il documento OpenAPI può essere scritto in YAML o in JSON.
OpenAPI può essere usato per i seguenti due scopi principali.

\begin{description}
    \item[Model-driven engineering] L'idea di utilizzare OpenAPI per una metodologia di sviluppo
    model-driven consiste nel creare, come primo artefatto del processo, il documento OpenAPI
    del proprio servizio REST~\cite{10.1145/3238147.3241536}. Una volta che l'interfaccia del
    servizio è stata definita con precisione, è possibile iniziare a sviluppare
    il codice del proprio servizio. Il grande supporto che viene dall'aver scritto un documento
    formale della propria interfaccia consiste nel poter usare dei generatori di codice, come
    \cite{swagger-codegen} e \cite{openapi-generator}. Essi supportano una varietà di linguaggi di
    programmazione e framework estremamente ampia e, prendendo in input il file contenente il documento
    OpenAPI, sono in grado di generare degli \emph{stub} per il servizio. Ai programmatori
    spetta ancora il compito di riempire gli \emph{stub} generati, sebbene vi siano alcuni lavori accademici
    che cercano di aumentare le capacità dei generatori di codice, per generare qualcosa più che semplici
    \emph{stub}~\cite{10.1145/3238147.3241536}.
    
    Questo approccio presenta due vantaggi. 
    Il primo sta nella riduzione di codice che i programmatori devono scrivere,
    il secondo sta nella garanzia che il codice generato aderisca al documento OpenAPI, a
    patto che il generatore di codice sia in grado di fornire questa garanzia.
    
    Di contro, come in tutte le metodologie model-driven, è richiesto un maggior sforzo
    iniziale, in questo caso nello scrivere l'interfaccia del proprio servizio.

    \item[Documentazione] Una volta che il documento OpenAPI è stato utilizzato per creare
    il codice del server, esso può servire come documentazione delle API esposte. Si può
    fornire ai client direttamente il documento OpenAPI, ma si possono sfruttare
    anche degli strumenti che, preso in input il documento, ne danno una visualizzazione grafica
    più \emph{human-readable}. Tra i più usati per tale scopo, si segnalano
    \emph{ReDoc}~\cite{redoc} e \emph{Swagger}~\cite{swagger-viewer}. Il primo
    è estremamente diffuso, ed utilizzato in progetti molto rilevanti, come
    la documentazione ufficiale delle API di Docker~\cite{docker-openapi}.
\end{description}

\subsubsection{Quando il documento manca}\label{subsec:api-driven-pre-req-openapi-when-doc-is-missing}
Vi possono essere casi in cui il servizio di API REST sia stato
sviluppato senza seguire un approccio model-driven. In queste situazioni può comunque
essere utile avere un documento OpenAPI, per ottenere tutti i benefici
derivanti dall'utilizzo di un documento formale per descrivere la propria interfaccia.
La prima strategia per farlo è scrivere il documento a mano, tuttavia si può fare di meglio.
In particolare, esistono delle librerie per alcuni web framework che consentono di generare
il documento OpenAPI \emph{a partire dal codice}. Tipicamente, tali librerie creano
una nuova API nel servizio REST, in cui servono il documento generato, in formato YAML o JSON.

Questa fase di generazione non è priva di errori né tanto meno semplice, per cui spesso
non è sufficiente importare la libreria di generazione del codice e niente più.
Può essere necessario dover annotare il proprio codice, o fare altre modifiche.
Molti framework supportano la generazione del documento a partire dal codice, come
\emph{MSF4J} per Java~\cite{swagger-gen-msf4j},
\emph{Laravel} per PHP~\cite{swagger-gen-laravel} e
\djangorest\ per Python~\cite{swagger-gen-drf}.

\subsubsection{Altri usi}\label{subsec:api-driven-pre-req-openapi-other-usage}
Il documento OpenAPI di un servizio REST è un documento che segue
una specifica formale, quindi fornisce una descrizione formale di tale servizio. Questo
fatto viene sfruttato per altri due utilizzi.

\begin{description}
    \item[Analisi] Vari lavori stanno esplorando il campo dell'analisi di un documento OpenAPI,
    sia come base per la validazione di un servizio REST, sia come base per la trasformazione
    in altri modelli. 
    Ad esempio, in~\cite{8536162} e \cite{karlsson2019quickrest} si presenta una metodologia per generare casi
    di test dato un documento.
    In~\cite{10.1007/978-3-319-91662-0_41} si presenta uno strumento per generare modelli UML
    a partire dal documento OpenAPI.
    \item[Generazione di codice lato client] Oltre a generare \emph{stub} lato server, i moderni generatori
    di codice per OpenAPI supportano anche la generazione di codice lato client. Essi sono
    in grado di generare un SDK completo, che astrae dal fatto che dietro le quinte si utilizzino
    delle richieste e delle risposte HTTP per interagire con il servizio. Ad esempio, essi
    generano delle classi che modellano le \restresources\ REST, e delle classi che modellano
    le \restoperations\ su tali \restresources.
    Tra i due generatori di codice più usati si segnalano\genopenapi~\cite{openapi-generator}
    e \genswagger~\cite{swagger-codegen}.
\end{description}

\subsubsection{Documento OpenAPI}\label{subsec:api-driven-pre-req-openapi-spec}
Un documento OpenAPI è composto dalle seguenti parti: \openapimetadata, \openapiservers,
\openapipaths, \openapiparameters, \openapirequest, \openapiresponses, \openapischemas,
\openapisecurityscheme, di seguito descritti nel dettaglio. Si noti che, per ciascun elemento,
si descrivono solo gli elementi principali~\cite{openapi-basic-structure, openapi2014openapi}.

\begin{description}
    \item[\openapimetadata] La sezione \openapimetadata\ funziona come una sorta di \emph{header}
    del documento OpenAPI, definendo a quale versione della specifica il documento OpenAPI
    è conforme. Mediante la chiave \openapitermkey{info}, si specifica la versione delle API REST
    che viene descritta, una descrizione, che può essere lunga più righe, ed un titolo.
    Un esempio è il seguente.
    \inputminted[lastline=7]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapiservers] La sezione \openapiservers\ definisce le URL del web server esponenti
    il servizio REST in questione. Non solo si specifica il nome del server, ma anche la URL di base,
    a cui vanno concatenate le URL definite nei \openapipaths. È possibile definire più di un server.
    \inputminted[firstline=8, lastline=11]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapipaths] La sezione \openapipaths\ definisce le diverse URL identificanti le \restresources\
    che il server espone. Le URL sono scritte in maniera relativa, ovvero specificando il percorso
    da appendere a quanto definito nella sezione \openapiservers. Per ciascun percorso, si definiscono
    i metodi HTTP ammissibili, andando quindi a definire le \restoperations.
    Ogni \restoperation\ si specifica il campo \key{operationId}, che definisce un nome univoco
    per l'operazione nell'intero documento (\codeline{20}).
    Ciascuna \restoperation\ può avere uno o più \key{tag}, utili per raggruppare le \restoperations\ tra
    loro; è usato in particolare dai visualizzatori (\codelines{25}{26}).
    \inputminted[firstline=12, lastline=26, highlightlines={13-14, 19-20, 24-26}]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapiparameters] La sezione \openapiparameters, all'interno di una certa \restoperation,
    descrive gli eventuali parametri della URL. Per ciascun \openapiparameter\ si possono specificare
    varie opzioni, come il tipo ed il valore minimo.
    \inputminted[firstline=44, lastline=69, highlightlines={45, 55-69}]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapirequest] La sezione \openapirequest, all'interno di una certa \restoperation,
    descrive l'eventuale \emph{body} della richiesta per l'\restoperation\ in questione. Va utilizzato solo
    per le \restoperations\ in cui c'è un \emph{body}, quindi non nelle \restoperations\ che usano il metodo
    \httpget. È possibile fornire una descrizione delle risposte differenziando per ogni
    formato supportato (ad esempio JSON, XML, YAML).
    \inputminted[firstline=79, lastline=100, highlightlines={88-100}]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapiresponses] La sezione \openapiresponses, all'interno di una certa \restoperation,
    descrive le risposte che vengono fornite. È possibile descrivere più risposte, una per ogni possibile
    \emph{status code}. Per ciascuna risposta poi è possibile fornirne una descrizione ulteriore
    una per ogni possibile formato. 
    \inputminted[firstline=101, lastline=108]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapischemas] La sezione \openapischemas, parte della sezione \emph{top-level} \openapicomponents,
    può essere usata per descrivere gli oggetti in input alle richieste, e ritornati dalle varie
    risposte. Il vantaggio di definire gli oggetti in una sezione a parte è che essi possono essere
    richiamati in altre parti del documento, evitando ripetizioni. Per richiamare un oggetto definito
    in un altro punto del documento si utilizza la chiave \key{\$ref}.
    Uno schema viene definito come segue.
    \inputminted[firstline=109, lastline=151, highlightlines={111-114, 134}]{yaml}{code/06_01_openapi_example.yml}
    Esso può essere richiamato come segue.
    \inputminted[firstline=101, lastline=108, highlightlines={108}]{yaml}{code/06_01_openapi_example.yml}

    \item[\openapisecurityscheme] La sezione \openapisecurityscheme\ viene usata per descrivere i
    metodi di autenticazione usati nelle API. In particolare, sotto la chiave \key{securitySchemes}\ si
    descrivono i vari schemi di autenticazione; per dichiarare che essi vengono effettivamente
    utilizzati occorre richiamarli nella chiave \key{security}, come segue.
    \inputminted[firstline=152, lastline=167, highlightlines={158, 167}]{yaml}{code/06_01_openapi_example.yml}
\end{description}

Per concludere, OpenAPI è un formato di descrizione per API REST che può essere usato per vari scopi.
Supporta una metodologia model-driven, in cui prima si definisce l'interfaccia delle proprie API, e da esse si sviluppa il
codice del server che le implementa. A partire dal documento OpenAPI è possibile anche generare del codice
lato client. Infine, il documento stesso può servire come documentazione delle API esposte.
