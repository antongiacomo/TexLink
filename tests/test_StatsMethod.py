import asyncio
import unittest

from TexLink.stats import Stats
from TexLink.url import Url


class TestStringMethods(unittest.TestCase):

    def test_stats(self):
        ret = [Url("http://google.it")]
        stats = Stats(ret)
        self.assertEqual(stats.online, 0)
        self.assertEqual(stats.redirect, 1)
        self.assertEqual(stats.offline, 1)
        with self.assertRaises(AttributeError):
            ret[0].is_online = 1

        ret = [asyncio.run(ret[0].get())]
        stats = Stats(ret)
        self.assertEqual(stats.online, 1)
        self.assertEqual(stats.redirect, 1)
        self.assertEqual(stats.offline, 0)


if __name__ == '__main__':
    unittest.main()
